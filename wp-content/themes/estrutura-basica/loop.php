<?php if ($wp_query->max_num_pages > 1) : ?>
    <nav id="nav-above" class="navigation">
        <div class="nav-previous"><?php next_posts_link(sprintf('<span class="meta-nav">&larr;</span> %s', 'Posts antigos')); ?></div>
        <div class="nav-next"><?php previous_posts_link(sprintf('%s <span class="meta-nav">&rarr;</span>', 'Posts recentes')); ?></div>
    </nav>
<?php endif; ?>

<?php if (!have_posts()) : ?>
    <article id="post-0" class="post error404 not-found">
        <header class="entry-title">
            <h1>Não encontrado</h1>
        </header>
        <div class="entry-content">
            <p>Desculpe, mas o arquivo requisitado não pôde ser encontrado, faça uma busca no site.</p>
        </div>
    </article>
<?php endif; ?>

<?php while (have_posts()) : the_post(); ?>
    <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
        <header class="entry-title">
            <h2><a href="<?php the_permalink(); ?>" title="Link para: <?php the_title(); ?>" rel="bookmark">
                <?php the_title(); ?>
            </a></h2>
        </header>

        <div class="entry-meta">
            <time><?php the_time('d/m/Y'); ?></time>
        </div>

        <?php if (is_archive() || is_search()) : ?>
            <div class="entry-summary">
                <?php the_excerpt(); ?>
            </div>
        <?php else : ?>
            <div class="entry-content">
                <?php the_content(sprintf('%s <span class="meta-nav">&rarr;</span>', 'Continuar lendo')); ?>
                <?php wp_link_pages(array('before' => sprintf('<div class="page-link">%s', 'Páginas:'), 'after' => '</div>')); ?>
            </div>
        <?php endif; ?>

        <div class="entry-utility">
            <?php if (count(get_the_category())) : ?>
                <span class="cat-links">
                    <?php printf('<span class="entry-utility-prep entry-utility-prep-cat-links">%1$s</span> %2$s', 'Postado em:', get_the_category_list(', ')); ?>
                </span>
            <?php endif; ?>
        </div>
    </article>
<?php endwhile;  ?>

<?php if ( $wp_query->max_num_pages > 1) : ?>
    <nav id="nav-below" class="navigation">
        <div class="nav-previous"><?php next_posts_link(sprintf('<span class="meta-nav">&larr;</span> %s', 'Posts antigos')); ?></div>
        <div class="nav-next"><?php previous_posts_link(sprintf('%s <span class="meta-nav">&rarr;</span>', 'Posts recentes')); ?></div>
    </nav>
<?php endif; ?>