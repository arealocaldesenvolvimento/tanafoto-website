<?= get_header() ?>

<section role="main" class="contato">
    <div class="container">
        <header>
            <h1>Contato</h1>
        </header>
        <div class="left-right-content">
            <div class="left">

                <div>
                    <a href="https://api.whatsapp.com/send?phone=5547988299949" target="_blank">
                        <span>
                            <div class="icon">
                                <img src="<?= get_image_url('/icons/whatsapp-logo-variant.svg') ?>" alt="">
                            </div>
                            <p>(47) 9 8829-9949</p>
                        </span>
                    </a>
                </div>

                <div>
                    <a href="mailto:contato@tanafoto.com.br">
                        <span>
                            <div class="icon">
                                <img src="<?= get_image_url('/icons/envelope.svg') ?>" alt="">
                            </div>
                            <p>contato@tanafoto.com.br</p>
                        </span>
                    </a>
                </div>
            </div>
            <div class="right">
                <?= do_shortcode('[contact-form-7 id="5" title="Formulário de contato"]') ?>
            </div>
        </div>
    </div>
</section>

<?= get_footer() ?>