<?php get_header() ?>

<section class="interna-noticias">
    <div class="container">

        <div class="left">
            <header>
                <span><?= get_the_date('d/m/Y', get_the_ID()) ?></span>
                <h1><?= the_title() ?></h1>
            </header>

            <div class="content">
                <main>
                    <div class="post-thumbnail">
                        <img src="<?=  get_thumbnail_url(get_the_ID(), 'full') ?>" alt="<?=  $post->post_title ?>"/>
                    </div>
                    <div class="conteudo-post">
                        <?= the_content(); ?>
                    </div>
                </main>

                <div class="comentarios">
                    <?= do_shortcode('[Fancy_Facebook_Comments]') ?>
                </div>
            </div>
        </div>

        <div class="right">
            <header>
                <h2>Onde ir?</h2>
            </header>
            <?php
            $query = new WP_Query(array(
                'post_type' => 'onde-ir',
                'post_status' => 'publish',
                'posts_per_page' => 2,
                'order' => 'DESC',
            ));
            ?>
            <div class="content">
                <?php if ($query->have_posts()) :
                    while ($query->have_posts()): $query->the_post(); ?>

                        <article class="card">
                            <a href="<?= get_the_permalink(get_the_ID()) ?>">
                                <img src="<?= get_the_post_thumbnail_url(get_the_ID(), 'medium_large') ?>" alt="<?= get_the_title(get_the_ID()) ?>" />
                                <div class="info">
                                    <span><?= strftime('%d de %B de %Y', strtotime(get_post_field('data', get_the_ID()))); ?></span>
                                    <h2><?= get_the_title(get_the_ID()) ?></h2>
                                </div>
                            </a>
                        </article>

                    <?php endwhile; ?>
                <?php endif; ?>
                <div class="anuncio">
                    <?= do_shortcode('[adrotate banner="8"]'); ?>
                </div>
            </div>
        </div>

    </div>
</section>

<?php get_footer() ?>
