<?php get_header() ?>

<?php
if(isset($_POST['cidade']) || isset($_POST['categoria']) || isset($_POST['dia']) ){
    $query = montaQuery($_POST['cidade'], $_POST['categoria'], $_POST['dia']);
} else {
    $query = new WP_Query(array(
        'post_type' => 'onde-ir',
        'post_status' => 'publish',
        'posts_per_page' => -1,
        'order' => 'ASC'
    ));
}

$categorias = get_terms( 'categoria', array(
    'hide_empty' => false,
));

$cidades = get_terms( 'cidade', array(
    'hide_empty' => false,
));

$dia_semana = get_terms( 'dia-semana', array(
    'hide_empty' => false,
    'orderby' => 'term_id',
    'order' => 'ASC'
));
?>

<section class="onde-ir">
    <div class="container">

        <div class="left">
            <div class="eventos">

                <header>
                    <h1>Onde Ir</h1>
                </header>
    
                <div class="content">
                    <div class="form-pesquisa">
                        <form action="" method="post">
                            <div>
                                <label for="cidade">Cidade</label>
                                <select name="cidade" id="cidade">
                                    <option value="">Todas as cidades</option>
                                    <?php if(!empty($cidades)): 
                                        foreach($cidades as $cidade):?>
                                            <option value="<?= $cidade->slug ?>"><?= $cidade->name ?></option>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                </select>
                            </div>
                            <div>
                                <label for="categoria">Categoria</label>
                                <select name="categoria" id="categoria">
                                    <option value="">Todas as categorias</option>
                                    <?php if(!empty($categorias)): 
                                        foreach($categorias as $categoria):?>
                                            <option value="<?= $categoria->slug ?>"><?= $categoria->name ?></option>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                </select>
                            </div>
                            <div>
                                <label for="mes">Dia da Semana</label>
                                <select name="dia" id="dia">
                                    <option value="">Todos os dias</option>
                                    <?php if(!empty($dia_semana)): 
                                        foreach($dia_semana as $dia):?>
                                            <option value="<?= $dia->slug ?>"><?= $dia->name ?></option>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                </select>
                            </div>
                            <div>
                                <button type="submit">Pesquisar</button>
                            </div>
                        </form>
                    </div>
                    <?php if ($query->have_posts()) :
                        while ($query->have_posts()): $query->the_post(); ?>
    
                        <article class="card-agenda">
                            <a href="<?= get_permalink(get_the_ID()) ?>">
                                <div>
                                    <?php if(get_post_field('imagem_miniatura', get_the_ID())): ?>
                                        <img src="<?= wp_get_attachment_image_src(get_post_field('imagem_miniatura', get_the_ID()), 'thumbnail')[0] ?>" alt="<?= get_the_title(get_the_ID()) ?>">
                                    <?php else: ?>
                                        <img src="<?= get_the_post_thumbnail_url(get_the_ID(), 'thumbnail') ?>" alt="<?= get_the_title(get_the_ID()) ?>" />
                                    <?php endif; ?>
                                </div>
                                <div class="info">
                                    <h2><?= get_the_title(get_the_ID()) ?></h2>
                                    <p><?= get_field('localizacao')['address'] ?></p>
    
                                    <div><?= get_the_excerpt(get_the_ID()) ?></div>
                                    <button>Saiba Mais!</button>
                                </div>
                            </a>
                        </article>
    
                        <?php endwhile ?>
                        <div style="clear: both;"></div>

                    <?php else: ?>
                        <p>Nenhum evento encontrado</p>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                    <?php endif ?>
                    <?php wp_reset_postdata(); ?>
                </div>
            </div>

        </div>

        <div class="right">
            <header>
                <h2>Promoções</h2>
            </header>

            <?php
            $query = new WP_Query(array(
                'post_type' => 'promocoes',
                'post_status' => 'publish',
                'posts_per_page' => 2,
                'order' => 'DESC',
            ));
            ?>
            <div class="content">
                <?php if ($query->have_posts()) :
                    while ($query->have_posts()): $query->the_post(); ?>

                        <article class="card">
                            <a href="<?= get_the_permalink(get_the_ID()) ?>">
                                <img src="<?= get_the_post_thumbnail_url(get_the_ID(), 'medium_large') ?>" alt="<?= get_the_title(get_the_ID()) ?>" />
                                <div class="info">
                                    <h2><?= get_the_title(get_the_ID()) ?></h2>
                                    <button type="button">Participar</button>
                                </div>
                            </a>
                        </article>

                    <?php endwhile; ?>
                <?php endif; ?>

                <div class="anuncio">
                    <?= do_shortcode('[adrotate banner="8"]'); ?>
                </div>
            </div>
        </div>

    </div>
</section>

<?php get_footer() ?>

