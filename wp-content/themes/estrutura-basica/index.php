<?php get_header(); ?>

<?php
$query = new WP_Query(array(
    'post_type' => array('post', 'coberturas'),
    'post_status' => 'publish',
    'posts_per_page' => 3,
    'meta_query' => array(
        'key' => 'area_destaque',
        'value' => '1',
        'compare' => '='
    ),
    'meta_key' => 'data',
    'orderby' => 'meta_value',
    'order' => 'DESC'
));
?>
<section class="destaque" role="main">
    <div class="container">
        <?php if ($query->have_posts()) :
            while ($query->have_posts()): $query->the_post(); ?>

            <article class="card">
                <a href="<?= get_the_permalink(get_the_ID()) ?>">
                    <img src="<?= get_the_post_thumbnail_url(get_the_ID(), 'medium_large') ?>" alt="<?= get_the_title(get_the_ID()) ?>" />
                    <div class="info">
                        <span><?= utf8_encode(strftime('%d de %B de %Y', strtotime(get_post_field('data', get_the_ID())))); ?></span>

                        <h2><?= get_the_title(get_the_ID()) ?></h2>
                        <button type="button">Veja mais</button>
                    </div>
                </a>
            </article>

            <?php endwhile ?>
            <div style="clear: both;"></div>
        <?php endif ?>
        <?php wp_reset_postdata(); ?>
    </div>
</section>

<section class="anuncio-centro">
    <div class="container">
        <?php
        // echo do_shortcode('[adrotate banner="3"]');
        ?>
        <div class="video">
            <a href="https://api.whatsapp.com/send?phone=5547988299949" target="_blank">
                <video autoplay muted loop>
                    <source src="<?= image_url('video.mp4') ?>" type="video/mp4">
                </video>
            </a>
        </div>
    </div>
</section>

<section class="agenda-home">
    <div class="container">
        <?php
        $query = new WP_Query(array(
            'post_type' => 'agenda',
            'post_status' => 'publish',
            'posts_per_page' => -1,
            'order_by' => 'meta_value_num',
            'meta_key' => 'data',
            'meta_query' => array(
                array(
                    'key' => 'data',
                    'value' => date('Y-m-d'),
                    'type' => 'date',
                    'compare' => '>=',
                ),
            ),
            'order' => 'ASC'
        ));

        $cont = 0;
        $postCount = $query->post_count;
        ?>
        <div class="agenda">
            <header>
                <h1>Agenda tá na foto</h1>
            </header>
            
            <div class="content">
                <div id="slider-agenda">
                    <div class="group-articles">
                        <?php if ($query->have_posts()) :
                            while ($query->have_posts()): $query->the_post(); 
                            $cont++;
                            ?>
        
                            <article class="card-agenda">
                                <div>
                                    <p><?= strftime('%d', strtotime(get_post_field('data', get_the_ID()))); ?></p>
                                    <p><?= strftime('%b', strtotime(get_post_field('data', get_the_ID()))); ?></p>
                                </div>
                                <div class="info">
                                    <a href="<?= get_home_url() ?>/agenda">
                                        <h2><?= get_the_title(get_the_ID()) ?></h2>
                                        <p><?= get_post_field('local', get_the_ID()) ?></p>
                                    </a>
                                </div>
                            </article>
                            <?php
                            if($cont % 3 == 0 && $postCount != $cont){
                                
                                echo "</div>";
                                echo "<div class='group-articles'>";
                            }
                            ?>
                            <?php endwhile ?>
                        <?php endif ?>
                        <?php wp_reset_postdata(); ?>
                    </div>
                </div>

            </div>
        </div>
        <div class="onde-ir">
            <header>
                <h1>Onde ir</h1>
            </header>

            <?php
            $query = new WP_Query(array(
                'post_type' => 'onde-ir',
                'post_status' => 'publish',
                'posts_per_page' => 2,
                'order' => 'DESC',
            ));
            ?>
            <div class="content">
                <?php if ($query->have_posts()) :
                    while ($query->have_posts()): $query->the_post(); ?>

                        <article class="card">
                            <a href="<?= get_the_permalink(get_the_ID()) ?>">
                                <img src="<?= get_the_post_thumbnail_url(get_the_ID(), 'medium_large') ?>" alt="<?= get_the_title(get_the_ID()) ?>" />
                                <div class="info">
                                    <span><?= strftime('%d de %B de %Y', strtotime(get_post_field('data', get_the_ID()))); ?></span>
                                    <h2><?= get_the_title(get_the_ID()) ?></h2>
                                </div>
                            </a>
                        </article>

                    <?php endwhile; ?>
                <?php endif; ?>
                <?php wp_reset_postdata(); ?>
            </div>

            <div>
                <a href="<?= get_home_url() ?>/onde-ir">Veja Mais Opções</a>
            </div>
        </div>

        <div class="anuncio">
            <?php
            echo do_shortcode('[adrotate banner="6"]');
            echo do_shortcode('[adrotate banner="7"]');
            ?>
        </div>
    </div>
</section>

<section class="promocoes-home">
    <?php
    $query = new WP_Query(array(
        'post_type' => 'promocoes',
        'post_status' => 'publish',
        'posts_per_page' => 3,
        'order' => 'DESC',
    ));
    ?>
    <div class="container">
        <header>
            <h1>Promoções</h1>
        </header>

        <div class="promocoes">
            <?php if ($query->have_posts()) :
                while ($query->have_posts()): $query->the_post(); ?>

                    <article class="card">
                        <a href="<?= get_the_permalink(get_the_ID()) ?>">
                            <img src="<?= get_the_post_thumbnail_url(get_the_ID(), 'medium_large') ?>" alt="<?= get_the_title(get_the_ID()) ?>" />
                            <div class="info">
                                <h2><?= get_the_title(get_the_ID()) ?></h2>
                                <button type="button">Participar</button>
                            </div>
                        </a>
                    </article>

                <?php endwhile; ?>
            <?php endif; ?>
        </div>
    </div>
</section>

<section class="anuncio-bottom">
    <div class="container">
        <?php
        echo do_shortcode('[adrotate banner="4"]');
        echo do_shortcode('[adrotate banner="5"]');
        ?>
    </div>
</section>

<?php get_footer(); ?>
