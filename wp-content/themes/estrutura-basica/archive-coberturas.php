<?php get_header() ?>

<?php
    // Define o locale para datas
    setlocale(LC_TIME, 'pt_BR', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese');
    date_default_timezone_set('America/Sao_Paulo');
    
    $paged = (get_query_var('paged')) ? absint(get_query_var('paged')) : 1;

    $query = new WP_Query(array(
        'post_type' => 'coberturas',
        'post_status' => 'publish',
        'posts_per_page' => 6,
        'meta_key' => 'data',
        'orderby' => 'meta_value',
        'order' => 'DESC',
        'paged' => $paged
    ));

?>
    <section class="coberturas">
        <div class="container">
            <header>
                <h1>Coberturas</h1>
            </header>

            <div class="content">
                <?php if ($query->have_posts()) :
                    while ($query->have_posts()): $query->the_post(); ?>

                    <article class="card">
                        <a href="<?= get_the_permalink(get_the_ID()) ?>">
                            <img src="<?= get_the_post_thumbnail_url(get_the_ID(), 'medium_large') ?>" alt="<?= get_the_title(get_the_ID()) ?>" />
                            <div class="info">
                                <span><?= utf8_encode(strftime('%d de %B de %Y', strtotime(get_post_field('data', get_the_ID())))); ?></span>

                                <h2><?= get_the_title(get_the_ID()) ?></h2>
                                <button type="button">Veja mais</button>
                            </div>
                        </a>
                    </article>

                    <?php endwhile; ?>
                <?php else: ?>
                    <p>Desculpe, não temos nenhuma cobertura cadastrada.</p>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                <?php endif ?>

                <?php wordpress_pagination($query) ?>
            </div>

        </div>
    </section>

<?php get_footer() ?>
