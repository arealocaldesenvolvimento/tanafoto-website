<?php get_header() ?>

<section class="interna-promocao">
    <input type="hidden" name="post_id" id="post_id" value="<?= the_ID() ?>">
    <div class="container">

        <div class="left">
            <header>
                <h1>Promoções</h1>
            </header>

            <div class="content">
                <main>
                    <div class="post-thumbnail">
                        <img src="<?=  get_thumbnail_url(get_the_ID(), 'full') ?>" alt="<?=  $post->post_title ?>"/>
                    </div>
                    <div class="conteudo-post">
                        <h2 class="promocao-title"><?= the_title() ?></h2>
                        <?= the_content(); ?>

                        <button class="btn-participar" id="btn-participar">Quero Participar!</button>
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100" preserveAspectRatio="xMidYMid" class="lds-rolling" style="background: none;"><circle cx="50" cy="50" fill="none" ng-attr-stroke="{{config.color}}" ng-attr-stroke-width="{{config.width}}" ng-attr-r="{{config.radius}}" ng-attr-stroke-dasharray="{{config.dasharray}}" stroke="#5e72e4" stroke-width="10" r="35" stroke-dasharray="164.93361431346415 56.97787143782138" transform="rotate(47.8541 50 50)"><animateTransform attributeName="transform" type="rotate" calcMode="linear" values="0 50 50;360 50 50" keyTimes="0;1" dur="1s" begin="0s" repeatCount="indefinite"></animateTransform></circle></svg>
                    </div>
                </main>
            </div>
        </div>

        <div class="right">
            <header>
                <h2>Onde ir?</h2>
            </header>
            <?php
            $query = new WP_Query(array(
                'post_type' => 'onde-ir',
                'post_status' => 'publish',
                'posts_per_page' => 2,
                'order' => 'DESC',
            ));
            ?>
            <div class="content">
                <?php if ($query->have_posts()) :
                    while ($query->have_posts()): $query->the_post(); ?>

                        <article class="card">
                            <a href="<?= get_the_permalink(get_the_ID()) ?>">
                                <img src="<?= get_the_post_thumbnail_url(get_the_ID(), 'medium_large') ?>" alt="<?= get_the_title(get_the_ID()) ?>" />
                                <div class="info">
                                    <span><?= strftime('%d de %B de %Y', strtotime(get_post_field('data', get_the_ID()))); ?></span>
                                    <h2><?= get_the_title(get_the_ID()) ?></h2>
                                </div>
                            </a>
                        </article>

                    <?php endwhile; ?>
                <?php endif; ?>
                <div class="anuncio">
                    <?= do_shortcode('[adrotate banner="8"]'); ?>
                </div>
            </div>
        </div>

    </div>
</section>

<?php get_footer() ?>
