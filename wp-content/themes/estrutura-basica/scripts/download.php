<?php
set_time_limit(0);
ignore_user_abort(false);
ini_set('output_buffering', 0);
ini_set('zlib.output_compression', 0);

$arquivoNome = filter_input(INPUT_GET, 'arquivo', FILTER_SANITIZE_URL);
$arquivoNome = urldecode($arquivoNome);

$partes = pathinfo($arquivoNome);

$extensoesPermitidas = array('jpg', 'png', 'jpeg', 'pdf', 'svg', 'doc', 'docx', 'xls', 'xlsx', 'csv', 'ppt', 'pptx');


if (!$partes['extension']  ||  !in_array($partes['extension'], $extensoesPermitidas) )
	die('Arquivo requisitado negado.');



$posicao = strrpos($arquivoNome, '/uploads/');
if ($posicao > -1) {
    $arquivoNome = str_replace('/uploads/', '', substr($arquivoNome, $posicao));
}

$arquivoLocal = '../../../uploads/'.$arquivoNome;
$aquivoNome   = explode('/', $arquivoNome);
$aquivoNome   = $arquivoNome[count($arquivoNome)-1];

if (!file_exists($arquivoLocal)) {
    $referencia = (isset($_SERVER['HTTP_REFERER'])) ? $_SERVER['HTTP_REFERER'] : false;

    if ($referencia) {
        echo 'Desculpe, mas o arquivo nao foi encontrado, voce esta sendo redirecionado para '.$referencia;
        echo '<script>location.href="'.$referencia.'";</script>';
    } else {
        echo 'Desculpe, mas nao conseguimos encontrar o arquivo requisitado';
    }

    exit;
}

header('Content-Description: File Transfer');
header('Content-Disposition: attachment; filename="'.$arquivoNome.'"');
header('Content-Type: application/octet-stream');
header('Content-Transfer-Encoding: binary');
header('Content-Length: ' . filesize($arquivoLocal));
header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
header('Pragma: public');
header('Expires: 0');

readfile($arquivoLocal);