<?php
// Carrega o wordpress
require_once('../../../../../wp-load.php');
global $wpdb;

// Pega os parâmetros passados
$post_id = filter_input(INPUT_POST, 'post_id', FILTER_SANITIZE_STRING);
$nome = filter_input(INPUT_POST, 'nome', FILTER_SANITIZE_STRING);
$email = filter_input(INPUT_POST, 'email', FILTER_SANITIZE_STRING);

$table = $wpdb->prefix.'promocao';
$data = array('post_id' => $post_id, 'nome' => $nome, 'email' => $email);
$format = array('%s','%d');

$wpdb->insert($wpdb->prefix.'promocao',array(
    'post_id' => $post_id, 
    'nome' => $nome, 
    'email' => $email
));


echo json_encode($wpdb->insert_id);
