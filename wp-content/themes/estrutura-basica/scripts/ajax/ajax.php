<?php
// Carrega o wordpress
require_once('../../../../../wp-load.php');

// Pega os parâmetros passados
$info = filter_input(INPUT_POST, 'info', FILTER_SANITIZE_STRING);

// Seta as posições de retorno
$retorno = array('erro' => null, 'mensagem' => null);

// Atualiza os valores
$retorno['erro'] = '404';
$retorno['mensagem'] = 'nada foi encontrado';

// Envia o retorno
echo json_encode($retorno);