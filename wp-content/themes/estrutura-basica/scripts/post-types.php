<?php
flush_rewrite_rules();
// Gerador: http://generatewp.com/post-type/

add_action('init', 'type_post_coberturas');

function type_post_coberturas() {
    $labels = array(
        'name' => _x('Cobertura', 'post type general name'),
        'singular_name' => _x('Cobertura', 'post type singular name'),
        'add_new' => _x('Adicionar Item', 'Novo item'),
        'add_new_item' => __('Adicionar Novo Item'),
        'edit_item' => __('Editar Item'),
        'new_item' => __('Novo Item'),
        'view_item' => __('Ver Item'),
        'search_items' => __('Procurar Itens'),
        'not_found' =>  __('Nenhum item encontrado'),
        'not_found_in_trash' => __('Nenhum item encontrado na lixeira'),
        'parent_item_colon' => '',
        'menu_name' => 'Cobertura'
    );

    $args = array(
        'labels' => $labels,
        'public' => true,
        'public_queryable' => true,
        'show_ui' => true,
        'query_var' => true,
        'rewrite' => true,
        'capability_type' => 'post',
        'has_archive' => true,
        'hierarchical' => false,
        'menu_position' => null,
        'supports' => array('title','thumbnail','custom-fields', 'revisions')
    );

    register_post_type( 'coberturas' , $args );
}

add_action('init', 'type_post_agenda');

function type_post_agenda() {
    $labels = array(
        'name' => _x('Agenda', 'post type general name'),
        'singular_name' => _x('Agenda', 'post type singular name'),
        'add_new' => _x('Adicionar Item', 'Novo item'),
        'add_new_item' => __('Adicionar Novo Item'),
        'edit_item' => __('Editar Item'),
        'new_item' => __('Novo Item'),
        'view_item' => __('Ver Item'),
        'search_items' => __('Procurar Itens'),
        'not_found' =>  __('Nenhum item encontrado'),
        'not_found_in_trash' => __('Nenhum item encontrado na lixeira'),
        'parent_item_colon' => '',
        'menu_name' => 'Agenda'
    );

    $args = array(
        'labels' => $labels,
        'public' => true,
        'public_queryable' => true,
        'show_ui' => true,
        'query_var' => true,
        'rewrite' => true,
        'capability_type' => 'post',
        'has_archive' => true,
        'hierarchical' => false,
        'menu_position' => null,
        'supports' => array('title','thumbnail','custom-fields', 'revisions', 'editor')
    );

    register_post_type( 'agenda' , $args );
}

add_action('init', 'type_post_promocoes');

function type_post_promocoes() {
    $labels = array(
        'name' => _x('Promoções', 'post type general name'),
        'singular_name' => _x('Promoção', 'post type singular name'),
        'add_new' => _x('Adicionar Item', 'Novo item'),
        'add_new_item' => __('Adicionar Novo Item'),
        'edit_item' => __('Editar Item'),
        'new_item' => __('Novo Item'),
        'view_item' => __('Ver Item'),
        'search_items' => __('Procurar Itens'),
        'not_found' =>  __('Nenhum item encontrado'),
        'not_found_in_trash' => __('Nenhum item encontrado na lixeira'),
        'parent_item_colon' => '',
        'menu_name' => 'Promoções'
    );

    $args = array(
        'labels' => $labels,
        'public' => true,
        'public_queryable' => true,
        'show_ui' => true,
        'query_var' => true,
        'rewrite' => true,
        'capability_type' => 'post',
        'has_archive' => true,
        'hierarchical' => false,
        'menu_position' => null,
        'supports' => array('title','thumbnail','custom-fields', 'revisions', 'editor')
    );

    register_post_type( 'promocoes' , $args );
}

add_action('init', 'type_post_onde_ir');

function type_post_onde_ir() {
    $labels = array(
        'name' => _x('Onde Ir', 'post type general name'),
        'singular_name' => _x('Onde Ir', 'post type singular name'),
        'add_new' => _x('Adicionar Item', 'Novo item'),
        'add_new_item' => __('Adicionar Novo Item'),
        'edit_item' => __('Editar Item'),
        'new_item' => __('Novo Item'),
        'view_item' => __('Ver Item'),
        'search_items' => __('Procurar Itens'),
        'not_found' =>  __('Nenhum item encontrado'),
        'not_found_in_trash' => __('Nenhum item encontrado na lixeira'),
        'parent_item_colon' => '',
        'menu_name' => 'Onde Ir'
    );

    $args = array(
        'labels' => $labels,
        'public' => true,
        'public_queryable' => true,
        'show_ui' => true,
        'query_var' => true,
        'rewrite' => true,
        'capability_type' => 'post',
        'has_archive' => true,
        'hierarchical' => false,
        'menu_position' => null,
        'supports' => array('title','thumbnail','custom-fields', 'revisions', 'editor')
    );
    register_post_type( 'onde-ir' , $args );

    $labels = array(
		'name'                       => _x( 'Categorias', 'Taxonomy General Name', 'text_domain' ),
		'singular_name'              => _x( 'Categoria', 'Taxonomy Singular Name', 'text_domain' ),
		'menu_name'                  => __( 'Categorias', 'text_domain' ),
		'all_items'                  => __( 'Todos os Itens', 'text_domain' ),
		'parent_item'                => __( 'Item Pai', 'text_domain' ),
		'parent_item_colon'          => __( 'Item Pai:', 'text_domain' ),
		'new_item_name'              => __( 'Novo Item', 'text_domain' ),
		'add_new_item'               => __( 'Adicionar item', 'text_domain' ),
		'edit_item'                  => __( 'Editar Item', 'text_domain' ),
		'update_item'                => __( 'Atualizar Item', 'text_domain' ),
		'view_item'                  => __( 'Ver Item', 'text_domain' ),
		'add_or_remove_items'        => __( 'Adicionar ou remover itens', 'text_domain' ),
		'search_items'               => __( 'Procurar Itens', 'text_domain' ),
		'not_found'                  => __( 'Não Encontrado', 'text_domain' ),
		'no_terms'                   => __( 'Sem Itens', 'text_domain' ),
		'items_list'                 => __( 'Lista', 'text_domain' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
	);
    register_taxonomy( 'categoria', array( 'onde-ir' ), $args );
    
    $labels = array(
		'name'                       => _x( 'Cidades', 'Taxonomy General Name', 'text_domain' ),
		'singular_name'              => _x( 'Cidade', 'Taxonomy Singular Name', 'text_domain' ),
		'menu_name'                  => __( 'Cidades', 'text_domain' ),
		'all_items'                  => __( 'Todos os Itens', 'text_domain' ),
		'parent_item'                => __( 'Item Pai', 'text_domain' ),
		'parent_item_colon'          => __( 'Item Pai:', 'text_domain' ),
		'new_item_name'              => __( 'Novo Item', 'text_domain' ),
		'add_new_item'               => __( 'Adicionar item', 'text_domain' ),
		'edit_item'                  => __( 'Editar Item', 'text_domain' ),
		'update_item'                => __( 'Atualizar Item', 'text_domain' ),
		'view_item'                  => __( 'Ver Item', 'text_domain' ),
		'add_or_remove_items'        => __( 'Adicionar ou remover itens', 'text_domain' ),
		'search_items'               => __( 'Procurar Itens', 'text_domain' ),
		'not_found'                  => __( 'Não Encontrado', 'text_domain' ),
		'no_terms'                   => __( 'Sem Itens', 'text_domain' ),
		'items_list'                 => __( 'Lista', 'text_domain' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
	);
    register_taxonomy( 'cidade', array( 'onde-ir' ), $args );
    
    $labels = array(
		'name'                       => _x( 'Dia Semana', 'Taxonomy General Name', 'text_domain' ),
		'singular_name'              => _x( 'Dia Semana', 'Taxonomy Singular Name', 'text_domain' ),
		'menu_name'                  => __( 'Dia Semana', 'text_domain' ),
		'all_items'                  => __( 'Todos os Itens', 'text_domain' ),
		'parent_item'                => __( 'Item Pai', 'text_domain' ),
		'parent_item_colon'          => __( 'Item Pai:', 'text_domain' ),
		'new_item_name'              => __( 'Novo Item', 'text_domain' ),
		'add_new_item'               => __( 'Adicionar item', 'text_domain' ),
		'edit_item'                  => __( 'Editar Item', 'text_domain' ),
		'update_item'                => __( 'Atualizar Item', 'text_domain' ),
		'view_item'                  => __( 'Ver Item', 'text_domain' ),
		'add_or_remove_items'        => __( 'Adicionar ou remover itens', 'text_domain' ),
		'search_items'               => __( 'Procurar Itens', 'text_domain' ),
		'not_found'                  => __( 'Não Encontrado', 'text_domain' ),
		'no_terms'                   => __( 'Sem Itens', 'text_domain' ),
		'items_list'                 => __( 'Lista', 'text_domain' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
	);
	register_taxonomy( 'dia-semana', array( 'onde-ir' ), $args );

}
