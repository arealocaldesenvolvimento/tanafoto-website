<?php
/**
 * Retorna o caminho completo da imagem
 * @param string $file nome do arquivo
 * @return string caminho completo do arquivo
 */
function get_image_url($file = '') {
    return get_template_directory_uri().'/assets/images/'.$file;
}

/**
 * Printa o caminho completo da imagem
 * @param string $file nome do arquivo
 */
function image_url($file = '') {
    echo get_image_url($file);
}

/**
 * Retorna a url completa da imagem original com o timthumb adicionado
 * @param string $url url da imagem original
 * @param string $params paramêtros adicionais
 * @return string url completa com timthumb
 */
function get_timthumb_url($url, $params = '') {
    $params = ($params) ? '&'.$params : '';
    return get_template_directory_uri().'/scripts/timthumb.php?src='.$url.$params;
}

/**
 * Printa a url completa da imagem original com o timthumb adicionado
 * @param string $url url da imagem original
 * @param string $params url completa com timthumb
 */
function timthumb_url($url, $params = '') {
    echo get_timthumb_url($url, $params);
}

/**
 * Printa a imagem da logo, se for a home coloca um wrap de h1
 * @param string $img imagem da logo
 */
function get_logo($img = 'logo.png') {
    $blogName = get_bloginfo('name');
    $tag = '<a href="%s" title="%s - %s" id="header-logo"><img src="%s" alt="%s"></a>';
    $tag = ($tag) ? "<h1 id='wrap-logo'>{$tag}</h1>" : '';
    $tag = sprintf($tag, home_url('/'), $blogName, get_bloginfo('description'), get_image_url($img), $blogName);

    echo $tag;
}

/**
 * Mostra um alert em javascript de arrays, strings, objetos etc..
 *
 * @param $msg string|array|object|int|boolean item a ser debugado
 * @return string tag javascript com um alert
 */
function msg($msg) {
    if ((is_object($msg)) || (is_array($msg))) {
        $msg = print_r($msg, true);
    } else if (is_bool($msg)) {
        if ($msg) {
            $msg = 'true';
        } else {
            $msg = 'false';
        }
    }

    $msg = str_replace(chr(13), '', $msg);
    $msg = str_replace(chr(10), '\n', $msg);

    if (strpos($msg, '"') !== false) {
        $msg = str_replace('"', '\"', $msg);
    }

    echo "<script> window.alert('{$msg}');</script>";
}

/**
 * Retorna a url da imagem destacada com timthumb
 *
 * @param $params string parametros do timthumb
 * @param $id int identificador do post desejado
 * @return array|bool|string url completa da imagem destacada
 */
function get_thumbnail($params = '', $id = null) {
    $thumb_id  = get_post_thumbnail_id($id);
    $thumbnail = wp_get_attachment_image_src($thumb_id, 'full');
    $thumbnail = (isset($thumbnail[0])) ? $thumbnail[0] : null;

    if (!$thumbnail)
        return '';

    if ($params && $thumbnail) {
        return get_timthumb_url($thumbnail, $params);
    } else {
        return $thumbnail;
    }
}

/*
 * Imprime a url da imagem destacada
 *
 * @param $params string parametros do timthumb
 * @param $id int identificador do post desejado
 */
function the_thumbnail($params = '', $id = null) {
    echo get_thumbnail($params, $id);
}

/**
 * Verfica se o url possui http / https e retorna ela corrigida
 *
 * @param $url string url a ser verificada
 * @param $padrao array|string a url que for igual a $padrao não será modificada
 * @return string url corrigida
 */
function verificaUrl($url, $padrao = array('#')) {
    if ($url == '#') {
        return '#';
    }

    $padrao = (!is_array($padrao)) ? array($padrao) : $padrao;
    if (in_array($url, $padrao)) {
        return $url;
    }

    $url = trim(strtolower($url));

    if (strrpos($url, 'http') === false) {
        $retorno = 'http://'.$url;
    } else {
        $retorno = $url;
    }

    return (filter_var($retorno, FILTER_VALIDATE_URL)) ? $retorno : get_home_url();
}

/*
 * Imprime o html da paginacao
 *
 * @param $obj WP_Query objeto a ser paginado
 * @param $base string url a ser paginada
 * @numLinks int numero de paginas que o html mostra
 * @return null|mixed
 */
function paginator($obj, $base, $numLinks = 3) {
    if ((int)$obj->max_num_pages === 1)
        return;

    $page = get_query_var('paged');
    $page = ($page) ? (int)$page : 1;

    $base = $base . '/page/';
    ?>
    <ul class="pagination">
        <li class="first <?php echo ($page == 1) ? ' disabled' : ''; ?>">
            <a href="<?php echo $base . '1'; ?>" class="button">
                <<
            </a>
        </li>
        <li class="prev <?php echo ($page == 1) ? ' disabled' : ''; ?>">
            <a href="<?php echo $base . ($page - 1); ?>" class="button">
                <
            </a>
        </li>
        <?php
        $half = ($obj->max_num_pages == 2) ? 1 : ($numLinks - 1) / 2;

        if ($page != 1) :
            if ($page == $obj->max_num_pages) :
                $half = $half * 2;
                $half = ($half >= $obj->max_num_pages) ? $obj->max_num_pages-1 : $half;
            endif;

            for ($i = $page - $half; $i < $page; $i++) :
                ?>
                <li class="page-<?php echo $i; ?>">
                    <a href="<?php echo $base . $i; ?>" class="button"><?php echo $i; ?></a>
                </li>
                <?php
            endfor;
        else :
            $half = $half * 2;
            $half = ($half >= $obj->max_num_pages) ? $obj->max_num_pages-1 : $half;
        endif;

        ?>
        <li class="page-<?php echo $page; ?> active">
            <a href="<?php echo $base . $page; ?>" class="button"><?php echo $page; ?></a>
        </li>
        <?php

        if ($page != $obj->max_num_pages) :
            for ($i = $page + 1; $i <= ($page + $half); $i++) :
                ?>
                <li class="page-<?php echo $i; ?>">
                    <a href="<?php echo $base . $i; ?>" class="button"><?php echo $i; ?></a>
                </li>
                <?php
            endfor;
        endif;
        ?>

        <li class="next <?php echo ($page < $obj->max_num_pages) ? '' : ' disabled'; ?>">
            <a href="<?php echo $base . ($page + 1); ?>" class="button">
                >
            </a>
        </li>
        <li class="last <?php echo ($page == $obj->max_num_pages) ? ' disabled' : ''; ?>">
            <a href="<?php echo $base . $obj->max_num_pages; ?>" class="button">
                >>
            </a>
        </li>
    </ul>
    <?php
}