<?php get_header() ?>
<!-- Para compartilhamento do facebook -->
<meta property="og:url" content="<?= get_permalink() ?>" />
<meta property="og:type"               content="article" />
<meta property="og:title"              content="<?= get_the_title(get_the_ID()) ?>" />
<meta property="og:description"        content="<?= get_the_excerpt(get_the_ID()) ?>" />
<meta property="og:image"              content="<?= get_the_post_thumbnail(get_the_ID(), 'thumbnail') ?>" />

<?php  
$location = get_field('localizacao');
?>
<section class="interna-onde-ir">
    <div class="container">

        <div class="left">
            <header>
                <h1>Onde Ir</h1>
            </header>

            <div class="content">
                <main>
                    <div class="post-thumbnail">
                        <img src="<?=  get_thumbnail_url(get_the_ID(), 'full') ?>" alt="<?=  $post->post_title ?>"/>
                    </div>
                    <div class="conteudo-post">
                        <h2 class="promocao-title"><?= the_title() ?></h2>
                        <?= the_content(); ?>

                    </div>
                    <?php if( $location ): ?>
                        <div class="acf-map" data-zoom="15">
                            <div class="marker" data-lat="<?php echo esc_attr($location['lat']); ?>" data-lng="<?php echo esc_attr($location['lng']); ?>"></div>
                        </div>
                    <?php endif; ?>
                </main>
            </div>
        </div>

        <div class="right">
            <header>
                <h2>Promoções</h2>
            </header>

            <?php
            $query = new WP_Query(array(
                'post_type' => 'promocoes',
                'post_status' => 'publish',
                'posts_per_page' => 2,
                'order' => 'DESC',
            ));
            ?>
            <div class="content">
                <?php if ($query->have_posts()) :
                    while ($query->have_posts()): $query->the_post(); ?>

                        <article class="card">
                            <a href="<?= get_the_permalink(get_the_ID()) ?>">
                                <img src="<?= get_the_post_thumbnail_url(get_the_ID(), 'medium_large') ?>" alt="<?= get_the_title(get_the_ID()) ?>" />
                                <div class="info">
                                    <h2><?= get_the_title(get_the_ID()) ?></h2>
                                    <button type="button">Participar</button>
                                </div>
                            </a>
                        </article>

                    <?php endwhile; ?>
                <?php endif; ?>

                <div class="anuncio">
                    <?= do_shortcode('[adrotate banner="8"]'); ?>
                </div>
            </div>
        </div>

    </div>
</section>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB0SLJ1b0MDhsYdkVM3KkiRr6L0qbHcT6E"></script>
</script>

<?php get_footer() ?>
