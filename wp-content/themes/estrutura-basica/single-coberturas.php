<?php get_header() ?>

<section class="interna-cobertura">
    <div class="container">

        <div class="left">
            <header>
                <span><?= strftime('%d/%m/%Y', strtotime(get_post_field('data', get_the_ID()))); ?></span>
                <h1><?= the_title() ?></h1>
            </header>

            <div class="content">
                <div id="galeria" style="display: none">
                    <?php if(!empty(get_post_field('galeria', get_the_ID()))): ?>
                        <?php foreach (get_post_field('galeria', get_the_ID()) as $id_imagem):?>
                            <div>
                                <img src="<?= wp_get_attachment_image_src($id_imagem, 'large')[0] ?>" alt="Imagem galeria">
                            </div>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </div>
                <div id="galeria-thumb" style="display: none">
                    <?php if(!empty(get_post_field('galeria', get_the_ID()))): ?>
                        <?php foreach (get_post_field('galeria', get_the_ID()) as $id_imagem):?>
                            <div>
                                <img src="<?= wp_get_attachment_image_src($id_imagem, 'thumbnail')[0] ?>" alt="Imagem galeria">
                            </div>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </div>

                <div class="buttons">
                    <a href="https://api.whatsapp.com/send?phone=5547988299949" target="_blank"><img src="<?= image_url('icons/btn-comprar.png') ?>" alt="Comprar Foto"></a>
                    <?= do_shortcode('[ssba-buttons]') ?>
                </div>

                <div class="comentarios">
                    <?= do_shortcode('[Fancy_Facebook_Comments]') ?>
                </div>
            </div>
        </div>

        <div class="right">
            <header>
                <h2>Onde ir?</h2>
            </header>
            <?php
            $query = new WP_Query(array(
                'post_type' => 'onde-ir',
                'post_status' => 'publish',
                'posts_per_page' => 2,
                'order' => 'DESC',
            ));
            ?>
            <div class="content">
                <?php if ($query->have_posts()) :
                    while ($query->have_posts()): $query->the_post(); ?>

                        <article class="card">
                            <a href="<?= get_the_permalink(get_the_ID()) ?>">
                                <img src="<?= get_the_post_thumbnail_url(get_the_ID(), 'medium_large') ?>" alt="<?= get_the_title(get_the_ID()) ?>" />
                                <div class="info">
                                    <span><?= strftime('%d de %B de %Y', strtotime(get_post_field('data', get_the_ID()))); ?></span>
                                    <h2><?= get_the_title(get_the_ID()) ?></h2>
                                </div>
                            </a>
                        </article>

                    <?php endwhile; ?>
                <?php endif; ?>
                <div class="anuncio">
                    <?= do_shortcode('[adrotate banner="8"]'); ?>
                </div>
            </div>
        </div>

    </div>
</section>

<?php get_footer() ?>
