<?php get_header() ?>

<?php
$query = new WP_Query(array(
    'post_type' => 'agenda',
    'post_status' => 'publish',
    'posts_per_page' => -1,
    'order_by' => 'meta_value_num',
    'meta_key' => 'data',
    'meta_query' => array(
        array(
            'key' => 'data',
            'value' => date('Y-m-d'),
            'type' => 'date',
            'compare' => '>=',
        ),
    ),
    'order' => 'ASC'
));


$queryPassados = new WP_Query(array(
    'post_type' => 'agenda',
    'post_status' => 'publish',
    'posts_per_page' => 3,
    'order_by' => 'meta_value_num',
    'meta_key' => 'data',
    'meta_query' => array(
        array(
            'key' => 'data',
            'value' => date('Y-m-d'),
            'type' => 'date',
            'compare' => '<',
        ),
    ),
    'order' => 'ASC'
));
?>

<section class="agenda">
    <div class="container">

        <div class="left">
            <div class="eventos">

                <header>
                    <h1>Agenda tá na foto</h1>
                </header>
    
                <div class="content">
                    <?php if ($query->have_posts()) :
                        while ($query->have_posts()): $query->the_post(); ?>
    
                        <article class="card-agenda">
                            <div>
                                <a href="#">
                                    <div>
                                        <img src="<?= get_the_post_thumbnail_url(get_the_ID(), 'full') ?>" alt="<?= get_the_title(get_the_ID()) ?>" />
                                    </div>
                                    <div>
                                        <p><?= strftime('%d', strtotime(get_post_field('data', get_the_ID()))); ?></p>
                                        <p><?= utf8_encode(strftime('%B', strtotime(get_post_field('data', get_the_ID())))); ?></p>
                                    </div>
                                </a>
                            </div>
                            <div class="info">
                                <a href="#">
                                    <h2><?= get_the_title(get_the_ID()) ?></h2>
                                    <p><?= get_post_field('local', get_the_ID()) ?></p>
                                    <h2>Atração:</h2>
                                    <p><?= get_post_field('atracao', get_the_ID()) ?></p>
    
                                    <div><?= get_the_content(get_the_ID()) ?></div>
                                </a>

                                <?php  
                                $link = get_field('saiba_mais', get_the_ID());
                                ?>
                                <?php if($link['link']){ ?>
                                    <a href="<?= $link['link'] ?>" target="_blank">Saiba mais!</a>
                                <?php } else if($link['imagem']){?>
                                    <a href="<?= wp_get_attachment_image_src($link['imagem'], 'full')[0] ?>" data-href="<?= wp_get_attachment_image_src($link['imagem'], 'full')[0] ?>" class="link-img-saibamais">Saiba mais!</a>
                                <?php } ?>
                            </div>
                        </article>
    
                        <?php endwhile ?>
                        <div style="clear: both;"></div>
                    <?php endif ?>
                    <?php wp_reset_postdata(); ?>
                </div>
            </div>

            <!-- Eventos passados -->
            <div class="eventos-anteriores">
                <header>
                    <h1>Eventos Anteriores</h1>
                </header>

                <div class="content">
                    <?php if ($queryPassados->have_posts()) :
                        while ($queryPassados->have_posts()): $queryPassados->the_post(); ?>
    
                        <article class="card-agenda">
                            <div>
                                <p><?= strftime('%d', strtotime(get_post_field('data', get_the_ID()))); ?></p>
                                <p><?= strftime('%b', strtotime(get_post_field('data', get_the_ID()))); ?></p>
                            </div>
                            <div class="info">
                                <h2><?= get_the_title(get_the_ID()) ?></h2>
                                <p><?= get_post_field('local', get_the_ID()) ?></p>
                            </div>

                            <?php if(get_post_field('vincular_cobertura', get_the_ID())): ?>
                                <a href="<?= get_the_permalink(get_post_field('vincular_cobertura', get_the_ID())[0]) ?>">Ver Cobertura</a>
                            <?php endif; ?>
                        </article>
    
                        <?php endwhile ?>
                        <div style="clear: both;"></div>
                    <?php endif ?>
                    <?php wp_reset_postdata(); ?>
                </div>
            </div>
        </div>

        <div class="right">
            <header>
                <h2>Promoções</h2>
            </header>

            <?php
            $query = new WP_Query(array(
                'post_type' => 'promocoes',
                'post_status' => 'publish',
                'posts_per_page' => 2,
                'order' => 'DESC',
            ));
            ?>
            <div class="content">
                <?php if ($query->have_posts()) :
                    while ($query->have_posts()): $query->the_post(); ?>

                        <article class="card">
                            <a href="<?= get_the_permalink(get_the_ID()) ?>">
                                <img src="<?= get_the_post_thumbnail_url(get_the_ID(), 'medium_large') ?>" alt="<?= get_the_title(get_the_ID()) ?>" />
                                <div class="info">
                                    <h2><?= get_the_title(get_the_ID()) ?></h2>
                                    <button type="button">Participar</button>
                                </div>
                            </a>
                        </article>

                    <?php endwhile; ?>
                <?php endif; ?>

                <div class="anuncio">
                    <?= do_shortcode('[adrotate banner="8"]'); ?>
                </div>
            </div>
        </div>

    </div>
</section>

<?php get_footer() ?>

