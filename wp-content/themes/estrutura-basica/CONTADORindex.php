<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="icon" href="<?php image_url('favicon.png'); ?>">

    <title>Tá na Foto - Sempre nos melhores Eventos</title>

    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/flipclock/compiled/flipclock.css">
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/assets/flipclock/compiled/flipclock.js"></script>
</head>
<body>
    <style>
        /* vietnamese */
        @font-face {
            font-family: 'Muli';
            font-style: normal;
            font-weight: 900;
            font-display: swap;
            src: url(https://fonts.gstatic.com/s/muli/v19/7Aulp_0qiz-aVz7u3PJLcUMYOFlnl0k40eiNxw.woff2) format('woff2');
            unicode-range: U+0102-0103, U+0110-0111, U+1EA0-1EF9, U+20AB;
        }
        /* latin-ext */
        @font-face {
            font-family: 'Muli';
            font-style: normal;
            font-weight: 900;
            font-display: swap;
            src: url(https://fonts.gstatic.com/s/muli/v19/7Aulp_0qiz-aVz7u3PJLcUMYOFlnl0k50eiNxw.woff2) format('woff2');
            unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
        }
        /* latin */
        @font-face {
            font-family: 'Muli';
            font-style: normal;
            font-weight: 900;
            font-display: swap;
            src: url(https://fonts.gstatic.com/s/muli/v19/7Aulp_0qiz-aVz7u3PJLcUMYOFlnl0k30eg.woff2) format('woff2');
            unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
        }
        *, *:before, *:after {
            box-sizing: border-box;
            font-family: 'Muli', sans-serif;
        }
        html {
            height: 100%;
            overflow: auto;
        }

        body {
            height: 100%;
            line-height: 1.6;
            width: 100%;
            margin: 0 auto;
            padding: 0;
            background-image: url("<?= image_url('background.png') ?>");
            background-position: center;
            background-size: cover;
            background-repeat: no-repeat;
        }
        .content{
            position: absolute;
            top: 50%;
            left: 50%;
            transform: translate(-50%, -50%);
            text-align: center;
            width: 40%;
        }
        .content .clock{
            margin: 0 auto !important;
            padding: 0;
            width: fit-content;
        }
        .content img{
            width: 180px;
        }
        .content h1{
            font-size: 50px;
            margin: 0 auto;
            padding-bottom: 60px;
        }
        @media screen and (max-width: 768px){
            .content{
                width: 100%;
            }
            .content h1{
                font-size: 30px;
            }
            .content img{
                width: 140px;
            }
        }
        @media screen and (max-width: 630px){
            .content .clock{
                zoom: 0.5;
            }
        }

        @media screen and (max-width: 1550px) and (min-width: 769px){
            .content{
                width: 86%;
            }
        }
    </style>

    <div class="content">
        <img src="<?= image_url('icon.png') ?>" alt="Tánafoto">
        <h1>Está chegando um novo Tá Na Foto</h1>
        <div class="clock" style="margin:2em;"></div>
    </div>
	<div class="message"></div>

	<script type="text/javascript">
		var clock;
		
		$(document).ready(function() {

            var date = new Date(2019, 11, 16, 18); //ano, mes, dia, hora. Mês começa em zero
            var now = new Date();
            var diff = (date.getTime()/1000) - (now.getTime()/1000);

            var clock = $('.clock').FlipClock(diff,{
                clockFace: 'DailyCounter',
                countdown: true
            });

            /* Corrige os labels para BR */
            $('.days > span.flip-clock-label').text('Dias');
            $('.hours > span.flip-clock-label').text('Horas');
            $('.minutes > span.flip-clock-label').text('Minutos');
            $('.seconds > span.flip-clock-label').text('Segundos');
		});
	</script>
</body>
</html>