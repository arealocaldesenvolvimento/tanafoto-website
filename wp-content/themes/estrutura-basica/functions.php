<?php
/*******************************************************
 * Includes
 *******************************************************/
include_once('scripts/helpers.php');
include_once('scripts/post-types.php');
include_once('scripts/shortcodes.php');

/*******************************************************
 * Actions e filters
 *******************************************************/
add_action('after_setup_theme', 'al_setup');
add_action( 'init', 'disable_wp_emojicons' );
add_action('show_admin_bar', '__return_false');
add_action('admin_head', 'show_panel_favicon');
add_action('widgets_init', 'add_widget_areas');
add_action('admin_enqueue_scripts', 'admin_scrips');
add_action('login_enqueue_scripts', 'change_login_form_logo');

remove_action('wp_head', 'wp_generator');

add_filter('widget_text', 'do_shortcode');
add_filter('login_errors', 'wrong_login');
add_filter('excerpt_more', 'change_excerpt_more');
add_filter('wp_page_menu_args', 'show_home_menu');
add_filter('login_headerurl', 'login_form_logo_url');
add_filter('excerpt_length', 'change_excerpt_length');
add_filter('sanitize_file_name', 'sanitize_filename', 10);
add_filter('admin_footer_text', 'change_panel_footer_text');
add_filter('login_headertext', 'login_form_logo_url_title');
add_filter('retrieve_password_message', 'reset_password_message', null, 2);
add_filter('wp_mail_content_type', 'wp_mail_return_texthtml');

/*******************************************************
 * Funções de configuração
 *******************************************************/

// Define o locale para datas
setlocale(LC_TIME, 'pt_BR', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese');
date_default_timezone_set('America/Sao_Paulo');


 function wp_mail_return_texthtml(){
    return "text/html";
 }
/**
 * Troca o texto no rodapé do painel
 */
function change_panel_footer_text() {
    echo '&copy; <a href="http://www.arealocal.com.br/" target="_blank">&Aacute;rea Local</a> - Web especializada';
}

/**
 * Adiciona favicon no painel
 */
function show_panel_favicon() {
    echo '<link href="'.get_image_url('favicon-panel.ico').'" rel="icon" type="image/x-icon">';
}

/**
 * Altera a logo do formulário de login
 */
function change_login_form_logo() {
    echo '<style>.login h1 a{background-image:url('.get_image_url('logo-login-form.png').')!important;}</style>';
}

/**
 * Altera a url da logo do formulário de login
 * @return string
 */
function login_form_logo_url() {
    return get_home_url();
}

/**
 * Altera title da logo do formulário de login
 * @return string
 */
function login_form_logo_url_title() {
    return get_bloginfo('name').' - Desenvolvido por Área Local';
}

/**
 * Adiciona navegação principal, suporte ao html5 e post thumbnail
 */
function al_setup() {
    register_nav_menus(array(
        'principal' => 'Navegação Principal'
    ));

    add_theme_support('post-thumbnails');
    add_theme_support('html5', array('comment-list', 'comment-form', 'search-form', 'gallery', 'caption'));
}

/**
 * Adiciona a home no menu
 * @param $args
 * @return mixed
 */
function show_home_menu($args)  {
    $args['show_home'] = true;
    return $args;
}

/**
 * Registra áreas de widget
 */
function add_widget_areas() {
    register_sidebar(array(
        'name' => 'Área de Widget Primária',
        'id' => 'area-widget-primaria',
        'description' => 'Área de Widget Primária',
        'before_widget' => '<li id="%1$s" class="widget-container %2$s">',
        'after_widget' => '</li>',
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>'
    ));

    register_sidebar(array(
        'name' => 'Área de Widget Secundária',
        'id' => 'area-widget-secundaria',
        'description' => 'Área de Widget Secundária',
        'before_widget' => '<li id="%1$s" class="widget-container %2$s">',
        'after_widget' => '</li>',
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>'
    ));
}

/**
 * Desabilita os emojis do wordpress
 */
function disable_wp_emojicons() {
    remove_action('admin_print_styles', 'print_emoji_styles');
    remove_action('wp_head', 'print_emoji_detection_script', 7);
    remove_action('admin_print_scripts', 'print_emoji_detection_script');
    remove_action('wp_print_styles', 'print_emoji_styles');
    remove_filter('wp_mail', 'wp_staticize_emoji_for_email');
    remove_filter('the_content_feed', 'wp_staticize_emoji');
    remove_filter('comment_text_rss', 'wp_staticize_emoji');
}

/**
 * Corrige bug de upload de arquivos com acentuação
 * @param $filename string nome do arquivo original
 * @return string nome do arquivo higienizado
 */
function sanitize_filename($filename) {
    $ext = explode('.', $filename);
    $ext = end($ext);
    $sanitized = preg_replace('/[^a-zA-Z0-9-_.]/', '', substr($filename, 0, -(strlen($ext) + 1)));
    $sanitized = str_replace('.', '-', $sanitized);

    if (function_exists('sanitize_title')) {
        $sanitized = sanitize_title($sanitized);
    }

    return strtolower($sanitized.'.'.$ext);
}

/**
 * Altera o tamanho padrão do excerpt em palavras
 * @param $length
 * @return int
 */
function change_excerpt_length($length) {
    return 25;
}

/**
 * Muda tag do excerpt
 * @return string
 */
function change_excerpt_more() {
    return '...';
}

/**
 * Padroniza mensagem de erro de login,
 * para não mostrar quando existe o usuário
 * @return string
 */
function wrong_login() {
    return '<b>ERRO</b>: Usuário ou senha incorretos.';
}

/**
 * Sobre escreve mensagem enviada por e-mail ao usário que quer recuperar a senha
 * a padrão do wordpress não envia o link para alteração
 * @param $message
 * @param $key
 * @return string
 */
function reset_password_message($message, $key) {
    if (strpos($_POST['user_login'], '@')) {
        $user_data = get_user_by('email', trim($_POST['user_login']));
    } else {
        $login     = trim($_POST['user_login']);
        $user_data = get_user_by('login', $login);
    }

    $user_login = $user_data->user_login;

    $msg = 'Recebemos um pedido de troca de senha para a conta abaixo em nosso site ('.get_bloginfo('name').'):<br /><br />';
    $msg .= 'Usuário: '.$user_login.'<br />';
    $msg .= 'Se você não solicitou, apenas ignore este e-mail.<br />';
    $msg .= 'Para trocar sua senha, acesse a seguinte url: '.home_url("wp-login.php?action=rp&key=$key&login=".rawurlencode($user_login), 'login');

    return $msg;
}

//Adiciona scrip admin.js e admin.css na tela do admin
function admin_scrips() {
    wp_enqueue_style('admin-styles', get_bloginfo("template_url").'/assets/css/admin.css');
    wp_enqueue_script('admin-functions', get_bloginfo("template_url").'/assets/js/admin.js');
}

//Traz a thumbnail, imagem do post ou img padrão
function get_thumbnail_url($post_id, $size){

    if( !isset($post_id)) {
        $post_id = get_the_ID();
    }
    if( has_post_thumbnail( $post_id ) ) {
        $post_thumbnail_url = get_the_post_thumbnail_url($post_thumbnail_id, $size);
    } else {
        $args = array(
            'numberposts' => 1,
            'order' => 'ASC',
            'post_mime_type' => 'image',
            'post_parent' => $post_id,
            'post_status' => null,
            'post_type' => 'attachment',
        );
        $post_thumbnail_url = array_values(get_children($args))[0]->guid;

        if (empty($post_thumbnail_url)) {
            $post_thumbnail_url = get_image_url('default.png');
        }
    }

    return $post_thumbnail_url;
}

//Corrige a paginação para usar em archives
function wordpress_pagination($query, $url = '') {
    global $wp_query;

    $big = 999999999;

    $url = (empty($url)) ? str_replace($big, '%#%', esc_url(get_pagenum_link($big))) : $url . 'page/%#%/';

    echo '<div class="pagination">';
    echo paginate_links( array(
        'base' => $url,
        'format' => '?paged=%#%',
        'current' => max( 1, get_query_var('paged') ),
        'next_text' => __('<i class="fas fa-arrow-right"></i>'),
        'prev_text' => __('<i class="fas fa-arrow-left"></i>'),
        'total' => $query->max_num_pages
    ) );
    echo '</div>';
}

/* Maps custom field */
function my_acf_google_map_api( $api ){
    $api['key'] = 'AIzaSyB0SLJ1b0MDhsYdkVM3KkiRr6L0qbHcT6E';
    return $api;
}
add_filter('acf/fields/google_map/api', 'my_acf_google_map_api');

function my_acf_init() {
    acf_update_setting('google_api_key', 'AIzaSyB0SLJ1b0MDhsYdkVM3KkiRr6L0qbHcT6E');
}
add_action('acf/init', 'my_acf_init');

//Monta a query de Onde Ir
function montaQuery($cidade, $categoria, $dia){
    $query = '';
    if($cidade != '' && $categoria == '' && $dia == ''){
        $query = new WP_Query(array(
            'post_type' => 'onde-ir',
            'post_status' => 'publish',
            'posts_per_page' => -1,
            'tax_query' => array(
                array(
                    'taxonomy' => 'cidade',
                    'field'    => 'slug',
                    'terms'    => $cidade,
                ),
            ),
            'order' => 'ASC'
        ));
    } else if($cidade == '' && $categoria != '' && $dia == ''){
        $query = new WP_Query(array(
            'post_type' => 'onde-ir',
            'post_status' => 'publish',
            'posts_per_page' => -1,
            'tax_query' => array(
                array(
                    'taxonomy' => 'categoria',
                    'field'    => 'slug',
                    'terms'    => $categoria,
                ),
            ),
            'order' => 'ASC'
        ));
    } else if($cidade == '' && $categoria == '' && $dia != ''){
        $query = new WP_Query(array(
            'post_type' => 'onde-ir',
            'post_status' => 'publish',
            'posts_per_page' => -1,
            'tax_query' => array(
                array(
                    'taxonomy' => 'dia-semana',
                    'field'    => 'slug',
                    'terms'    => $dia,
                ),
            ),
            'order' => 'ASC'
        ));
    } else if($cidade != '' && $categoria != '' && $dia != ''){
        $query = new WP_Query(array(
            'post_type' => 'onde-ir',
            'post_status' => 'publish',
            'posts_per_page' => -1,
            'tax_query' => array(
                'relation' => 'AND',
                array(
                    'taxonomy' => 'dia-semana',
                    'field'    => 'slug',
                    'terms'    => $dia,
                ),
                array(
                    'taxonomy' => 'categoria',
                    'field'    => 'slug',
                    'terms'    => $categoria,
                ),
                array(
                    'taxonomy' => 'cidade',
                    'field'    => 'slug',
                    'terms'    => $cidade,
                ),
            ),
            'order' => 'ASC'
        ));
    } else if($cidade != '' && $categoria != '' && $dia == ''){
        $query = new WP_Query(array(
            'post_type' => 'onde-ir',
            'post_status' => 'publish',
            'posts_per_page' => -1,
            'tax_query' => array(
                'relation' => 'AND',
                array(
                    'taxonomy' => 'categoria',
                    'field'    => 'slug',
                    'terms'    => $categoria,
                ),
                array(
                    'taxonomy' => 'cidade',
                    'field'    => 'slug',
                    'terms'    => $cidade,
                ),
            ),
            'order' => 'ASC'
        ));
    } else if($cidade == '' && $categoria != '' && $dia != ''){
        $query = new WP_Query(array(
            'post_type' => 'onde-ir',
            'post_status' => 'publish',
            'posts_per_page' => -1,
            'tax_query' => array(
                'relation' => 'AND',
                array(
                    'taxonomy' => 'dia-semana',
                    'field'    => 'slug',
                    'terms'    => $dia,
                ),
                array(
                    'taxonomy' => 'categoria',
                    'field'    => 'slug',
                    'terms'    => $categoria,
                ),
            ),
            'order' => 'ASC'
        ));
    } else if($cidade != '' && $categoria == '' && $dia != ''){
        $query = new WP_Query(array(
            'post_type' => 'onde-ir',
            'post_status' => 'publish',
            'posts_per_page' => -1,
            'tax_query' => array(
                'relation' => 'AND',
                array(
                    'taxonomy' => 'dia-semana',
                    'field'    => 'slug',
                    'terms'    => $dia,
                ),
                array(
                    'taxonomy' => 'cidade',
                    'field'    => 'slug',
                    'terms'    => $cidade,
                ),
            ),
            'order' => 'ASC'
        ));
    }

    return $query;
}


