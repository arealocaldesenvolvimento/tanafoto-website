<?php get_header() ?>

<?php
    $paged = (get_query_var('paged')) ? absint(get_query_var('paged')) : 1;

    $query = new WP_Query(array(
        'post_type' => 'promocoes',
        'post_status' => 'publish',
        'posts_per_page' => 6,
        'order' => 'DESC',
        'paged' => $paged
    ));

?>
    <section class="promocoes">
        <div class="container">
            <header>
                <h1>Promoções</h1>
            </header>

            <div class="content">
                <?php if ($query->have_posts()) :
                    while ($query->have_posts()): $query->the_post(); ?>

                    <article class="card">
                        <a href="<?= get_the_permalink(get_the_ID()) ?>">
                            <img src="<?= get_the_post_thumbnail_url(get_the_ID(), 'medium') ?>" alt="<?= get_the_title(get_the_ID()) ?>" />
                            <div class="info">

                                <h2><?= get_the_title(get_the_ID()) ?></h2>
                                <button type="button">Participar</button>
                            </div>
                        </a>
                    </article>

                    <?php endwhile; ?>
                <?php else: ?>
                    <p>Desculpe, não temos nenhuma promoção no momento.</p>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                <?php endif ?>

                <?php wordpress_pagination($query) ?>
            </div>

        </div>
    </section>

<?php get_footer() ?>
