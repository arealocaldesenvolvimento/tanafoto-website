<?php get_header(); ?>

    <section class="container" role="main">
        <?php while (have_posts()) : the_post(); ?>
            <article id="id-<?php the_ID(); ?>" <?php post_class(); ?>>
                <header class="entry-title">
                    <h1><?php the_title(); ?></h1>
                </header>

                <div class="entry-content">
                    <?php the_content(); ?>
                </div>
            </article>
        <?php endwhile; ?>
    </section>

<?php get_footer(); ?>