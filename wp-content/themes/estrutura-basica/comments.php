<section id="comments">
    <?php if (is_singular()) wp_enqueue_script('comment-reply'); ?>

    <?php if (have_comments()) : ?>
        <header>
            <h3 id="comments-title">
                <?php printf(
                    _n('Uma resposta para <em>%2$s</em>>', '%1$s resposta para <em>%2$s</em>', get_comments_number()),
                    number_format_i18n(get_comments_number()), get_the_title()
                ); ?>
            </h3>
        </header>
        <?php if (get_comment_pages_count() > 1 && get_option('page_comments')) : ?>
            <nav class="navigation" role="links">
                <div class="nav-previous"><?php previous_comments_link(sprintf('<span class="meta-nav">&larr;</span> %s', 'Coment�rios Antigos')); ?></div>
                <div class="nav-next"><?php next_comments_link(sprintf('%s <span class="meta-nav">&rarr;</span>', 'Coment�rios Recentes')); ?></div>
            </nav>
        <?php endif; ?>

        <ol class="commentlist">
            <?php wp_list_comments(); ?>
        </ol>

        <?php if (get_comment_pages_count() > 1 && get_option('page_comments')) : ?>
            <nav class="navigation" role="links">
                <div class="nav-previous"><?php previous_comments_link(sprintf('<span class="meta-nav">&larr;</span> %s', 'Coment�rios antigos')); ?></div>
                <div class="nav-next"><?php next_comments_link(sprintf('%s <span class="meta-nav">&rarr;</span>', 'Coment�rios recentes')); ?></div>
            </nav>
        <?php endif; ?>

    <?php elseif (!comments_open()) : ?>
        <p class="nocomments">Voc� n�o pode comentar nessa p�gina.</p>
    <?php endif; ?>

    <?php comment_form(); ?>
</section>