    </div>

    <div id="footer-container">
        <footer id="main-footer">

            <div class="container">
                <div class="logo-footer">
                    <img src="<?= image_url('logo-branca.png') ?>" alt="Tanafoto">
                    <a href="https://api.whatsapp.com/send?phone=5547988299949" target="_blank"><i class="fas fa-phone-alt"></i> (47) 98829-9949</a>
                    <a href="mailto:contato@tanafoto.com.br"><i class="fas fa-envelope"></i> contato@tanafoto.com.br</a>
                </div>
                <div class="sociais">
                    <h4>Acompanhe a gente</h4>
                    <a href="https://www.facebook.com/tanafoto.com.br/" target="_blank"><img src="<?= image_url('icons/facebook-footer.svg') ?>" alt="Facebook"></a>
                    <a href="https://www.instagram.com/tanafoto.com.br/" target="_blank"><img src="<?= image_url('icons/instagram-footer.svg') ?>" alt="Instagram"></a>
                    <a href="https://twitter.com/tanafotosite" target="_blank"><img src="<?= image_url('icons/twitter-footer.svg') ?>" alt="Twitter"></a>
                </div>
                <div class="mapa-site">
                    <h4>Mapa do Site</h4>
                    <?php wp_nav_menu(array('theme_location' => 'principal', 'container' => false)); ?>
                </div>
                <div class="anunciar">
                    <h4>Como Anunciar?</h4>
                    <a href="<?= get_home_url() ?>/como-anunciar">
                        <span>Anuncie aqui!</span>
                        <span>Conheça nossas opções e divulgue sua marca!</span>
                    </a>
                </div>
            </div>

            <div class="link-al-footer">
                <div class="container">
                    <a href="http://www.arealocal.com.br" target="_blank" title="Desenvolvido por: Área Local" class="section-footer">Desenvolvido por: Área Local</a>
                </div>
            </div>

        </footer>
    </div>

    <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/js/libs/jquery-2.1.3.min.js"></script>
    <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/js/libs/slick.min.js"></script>


    <script>/*<![CDATA[*/var al_url={"templateUrl":"<?php echo addslashes(get_bloginfo('template_url')); ?>","homeUrl":"<?php echo addslashes(home_url()); ?>"};/*]]>*/</script>
    <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/js/libs/sweetalert2.all.min.js"></script>
    <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/js/funcoes/funcoes.js" async></script>
    <!-- <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/js/funcoes.min.js" async></script> -->

    <!-- Menu Mobile -->
    <script>
        function openNav() {
            document.getElementById("mySidenav").style.width = "250px";
        }

        function closeNav() {
            document.getElementById("mySidenav").style.width = "0";
        }
    </script>
    <?php wp_footer(); ?>
</body>
</html>
