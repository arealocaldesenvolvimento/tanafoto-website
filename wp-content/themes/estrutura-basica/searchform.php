<form id="search-form" role="search" method="get" action="<?php echo home_url('/'); ?>">
    <input type="search" value="" name="s" id="s" placeholder="Faça uma busca" />
    <button id="search-submit">
        <i class="fas fa-search"></i>
    </button>
</form>
