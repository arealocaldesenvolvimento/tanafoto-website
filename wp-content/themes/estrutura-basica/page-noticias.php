<?php get_header() ?>

<?php
    // Define o locale para datas
    setlocale(LC_TIME, 'pt_BR', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese');
    date_default_timezone_set('America/Sao_Paulo');
    
    $paged = (get_query_var('paged')) ? absint(get_query_var('paged')) : 1;

    $query = new WP_Query(array(
        'post_type' => 'post',
        'post_status' => 'publish',
        'posts_per_page' => 3,
        'order' => 'DESC',
        'paged' => $paged
    ));

?>
    <section class="noticias">
        <div class="container">

            <div class="left">
                <header>
                    <h1>Notícias</h1>
                </header>

                <div class="content">
                    <?php if ($query->have_posts()) :
                        while ($query->have_posts()): $query->the_post(); ?>

                        <article class="card">
                            <a href="<?= get_the_permalink(get_the_ID()) ?>">
                                <img src="<?= get_the_post_thumbnail_url(get_the_ID(), 'medium_large') ?>" alt="<?= get_the_title(get_the_ID()) ?>" />
                                <div class="info">
                                    <span><?= get_the_date('d', get_the_ID()).' de '.get_the_date('F', get_the_ID()).' de '.get_the_date('Y', get_the_ID()) ?></span>
                                    
                                    <h2><?= get_the_title(get_the_ID()) ?></h2>

                                    <p><?= get_the_excerpt(get_the_ID()) ?></p>
                                    
                                    <button type="button">Veja mais</button>
                                </div>
                            </a>
                        </article>

                        <?php endwhile; ?>
                    <?php else: ?>
                        <p>Desculpe, não temos nenhuma cobertura cadastrada.</p>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                    <?php endif ?>

                    <?php wordpress_pagination($query) ?>
                </div>
            </div>

            <div class="right">
                <header>
                    <h2>Onde ir?</h2>
                </header>
                <?php
                $query = new WP_Query(array(
                    'post_type' => 'onde-ir',
                    'post_status' => 'publish',
                    'posts_per_page' => 2,
                    'order' => 'DESC',
                ));
                ?>
                <div class="content">
                    <?php if ($query->have_posts()) :
                        while ($query->have_posts()): $query->the_post(); ?>

                            <article class="card">
                                <a href="<?= get_the_permalink(get_the_ID()) ?>">
                                    <img src="<?= get_the_post_thumbnail_url(get_the_ID(), 'medium_large') ?>" alt="<?= get_the_title(get_the_ID()) ?>" />
                                    <div class="info">
                                        <span><?= strftime('%d de %B de %Y', strtotime(get_post_field('data', get_the_ID()))); ?></span>
                                        <h2><?= get_the_title(get_the_ID()) ?></h2>
                                    </div>
                                </a>
                            </article>

                        <?php endwhile; ?>
                    <?php endif; ?>
                    <div class="anuncio">
                        <?= do_shortcode('[adrotate banner="8"]'); ?>
                    </div>
                </div>
            </div>
            
        </div>
    </section>

<?php get_footer() ?>
