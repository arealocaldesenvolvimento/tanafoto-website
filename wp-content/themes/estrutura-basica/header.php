<!DOCTYPE html>
<html lang="pt-BR" xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="UTF-8" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="initial-scale=1">
    <meta name="web_author" content="Área Local">
    <title><?php wp_title(''); ?></title>
    <link rel="icon" href="<?php image_url('favicon.png'); ?>">
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
    <link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo('stylesheet_url'); ?>">

    <!-- Font awesome -->
    <link rel="stylesheet" type="text/css" media="all" href="<?php echo get_template_directory_uri(); ?>/assets/css/libs/all.min.css">
    <!-- Sweet Alert -->
    <link rel="stylesheet" type="text/css" media="all" href="<?php echo get_template_directory_uri(); ?>/assets/css/libs/sweetalert2.min.css">

    <script type="text/javascript">
        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-7960603-1']);
        _gaq.push(['_trackPageview']);

        (function() {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();
    </script>

    <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
    <div id="header-container">

        <div class="top-bar">
            <div class="container">
                <div>
                    <a href="<?= get_home_url() ?>/wp-login.php">Fazer Login</a>
                </div>
                <div>
                    <a href="https://www.facebook.com/tanafoto.com.br/" target="_blank"><i class="fa fa-facebook-f"></i></a>
                    <a href="https://www.instagram.com/tanafoto.com.br/" target="_blank"><i class="fab fa-instagram"></i></a>
                    <a href="https://twitter.com/tanafotosite" target="_blank"><i class="fab fa-twitter"></i></a>
                </div>
            </div>
        </div>
        <header id="main-header">
            <div class="container">
                <?php get_logo(); ?>

                <nav id="main-menu">
                    <?php wp_nav_menu(array('theme_location' => 'principal', 'container' => false)); ?>
                    <div class="search">
                        <?= get_search_form() ?>
                    </div>
                </nav>
            </div>
        </header>

        <!-- Menu Mobile -->

        <div class="container" id="container-menu-mobile">
            <div id="mySidenav" class="sidenav">
                <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
                <?php  
                $menuItems = wp_get_nav_menu_items('principal');

                foreach($menuItems as $item): ?>
                    <a href="<?= $item->url ?>"><?= $item->title ?></a>
                <?php endforeach; ?>
            </div>
            <span class="icon-menu" onclick="openNav()"><i class="fas fa-bars"></i></span>

            <div class="logo-mobile">
                <?php get_logo(); ?>
            </div>
        </div>

    </div>

    <div id="wrapper">
