$(function() {
    if (!document.addEventListener) {
        window.location = 'http://www.arealocal.com.br/ie6/oculta-site-ie6.php';
    }

    var templateUrl = al_url['templateUrl'];
    var homeUrl = al_url['homeUrl'];
});

$(document).ready(function(){
    var templateUrl = al_url['templateUrl'];
    var homeUrl = al_url['homeUrl'];

    /* Galeria de eventos na agenda da home */
    $('#slider-agenda').on('init', function(event, slick){
        $('#slider-agenda').css({'display':'block'});
    });
    $('#slider-agenda').slick({
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        dots: true
    });

    /* Galeria de coberturas*/
    $('#galeria').on('init', function(event, slick){
        $(this).css({'display':'block'});
    });
    $('#galeria').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        fade: true,
        asNavFor: '#galeria-thumb',
        arrows: true,
        prevArrow: '<span class="slick-prev"><img src="'+templateUrl+'/assets/images/icons/left-arrow.png"></span>',
        nextArrow: '<span class="slick-next"><img src="'+templateUrl+'/assets/images/icons/right-arrow.png"></span>'
    });

    $('#galeria-thumb').on('init', function(event, slick){
        $(this).css({'display':'block'});
    });
    $('#galeria-thumb').slick({
        slidesToShow: 4,
        slidesToScroll: 1,
        asNavFor: '#galeria',
        dots: false,
        centerMode: true,
        focusOnSelect: true,
        arrows: true,
        prevArrow: '<span class="slick-prev"><img src="'+templateUrl+'/assets/images/icons/left-arrow-sm.png"></span>',
        nextArrow: '<span class="slick-next"><img src="'+templateUrl+'/assets/images/icons/right-arrow-sm.png"></span>',
        responsive:[
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1,
                }
            },
            {
                breakpoint: 680,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1,
                }
            },
            {
                breakpoint: 425,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                }
            }
        ]
    });

    /* 
    * Sweet alert para o saiba mais da agenda
    */

    $('.link-img-saibamais').click(function(e){
        e.preventDefault();
        var img = $(this).data('href');
        Swal.fire({
            title: '',
            text: '',
            imageUrl: img,
            imageWidth: 400,
            imageAlt: 'Flyer Evento',
        });
    });

    /* MAPS interna Onde Ir */
    var divMap = $('.acf-map');
    if(divMap.length > 0){
        initMap(divMap);
    }

    /* Participar das promoções */
    var btn_participar = $('#btn-participar');
    if(btn_participar.length > 0){
        initSdkFacebook();

        $('#btn-participar').click(function(){
            var href = window.location.href;
            var nome = '';
            var email = '';
            var post_id = $('input#post_id').val();

            $(this).prop('disabled', true);
            $(this).css({'opacity':'0.5'});
            $(this).siblings('svg').css({'display':'inline'});

            //Solicita login com nome e email
            FB.login(function(response){
                if (response.status === 'connected') {

                    FB.api(
                        '/me',
                        'GET',
                        {'fields':'name,email'},
                        function (response2) {
                            if (response2 && !response2.error) {
                                nome = response2.name;
                                email = response2.email;

                                FB.ui({
                                    method: 'share',
                                    href: href,
                                    redirect_uri: href,
                                }, function(response){
                                    if(response.length != 0 && response.error_code == 4201){
                                        Swal.fire({
                                            title: 'Oops',
                                            text: "Você precisa compartilhar para poder participar da promoção",
                                            icon: 'warning',
                                            allowOutsideClick: false,
                                            showCancelButton: true,
                                            confirmButtonColor: '#3085d6',
                                            cancelButtonColor: '#d33',
                                            confirmButtonText: 'Entendi',
                                            cancelButtonText: 'Não quero participar'
                                        }).then(function(result){
                                            if (result.value) {
                                                location.reload();
                                            } else {
                                                $('#btn-participar').siblings('svg').css({'display':'none'});
                                            }
                                        });
                                    } else {
                                        insereParticipante(nome, email, post_id);
                                    }
                                });
                            }
                        }
                    );
                }
            },{scope: 'public_profile,email'});

        });
    }
    
});
/* Variáveis globais */
var templateUrl = al_url['templateUrl'];
var homeUrl = al_url['homeUrl'];

//Função que insere o participante da promoção via ajax
function insereParticipante(nome, email, post_id){
    $.post(templateUrl+'/scripts/ajax/promocao.php', {post_id:post_id, nome:nome, email:email})
    .done(function(res){
        if(res != '' && res != 0){
            Swal.fire({
                icon: 'success',
                title: 'Tudo certo!',
                text: 'Seu nome e e-mail foram registrados na promoção'
            });
            $('#btn-participar').siblings('svg').css({'display':'none'});
        } else {
            Swal.fire({
                icon: 'error',
                title: 'Erro',
                text: 'Nao foi possível realizar o cadastro na promoção.'
            });
        }
    });
}

//Instancia sdk do facebook
function initSdkFacebook(){
    $.ajaxSetup({ cache: true });
    $.getScript('https://connect.facebook.net/en_US/sdk.js', function(){
        FB.init({
            appId: '648380565688626',
            version: 'v4.0'
        });
    });
}

function initMap( $el ) {

    // Find marker elements within map.
    var $markers = $el.find('.marker');

    // Create gerenic map.
    var mapArgs = {
        zoom        : $el.data('zoom') || 16,
        mapTypeId   : google.maps.MapTypeId.ROADMAP,
        disableDefaultUI: true
    };
    var map = new google.maps.Map( $el[0], mapArgs );

    // Add markers.
    map.markers = [];
    $markers.each(function(){
        initMarker( $(this), map );
    });

    // Center map based on markers.
    centerMap( map );

    // Return map instance.
    return map;
}

function initMarker( $marker, map ) {

    // Get position from marker.
    var lat = $marker.data('lat');
    var lng = $marker.data('lng');
    var latLng = {
        lat: parseFloat( lat ),
        lng: parseFloat( lng )
    };

    // Create marker instance.
    var marker = new google.maps.Marker({
        position : latLng,
        map: map
    });

    // Append to reference for later use.
    map.markers.push( marker );

    // If marker contains HTML, add it to an infoWindow.
    if( $marker.html() ){

        // Create info window.
        var infowindow = new google.maps.InfoWindow({
            content: $marker.html()
        });

        // Show info window when marker is clicked.
        google.maps.event.addListener(marker, 'click', function() {
            infowindow.open( map, marker );
        });
    }
}

function centerMap( map ) {

    // Create map boundaries from all map markers.
    var bounds = new google.maps.LatLngBounds();
    map.markers.forEach(function( marker ){
        bounds.extend({
            lat: marker.position.lat(),
            lng: marker.position.lng()
        });
    });

    // Case: Single marker.
    if( map.markers.length == 1 ){
        map.setCenter( bounds.getCenter() );

    // Case: Multiple markers.
    } else{
        map.fitBounds( bounds );
    }
}
