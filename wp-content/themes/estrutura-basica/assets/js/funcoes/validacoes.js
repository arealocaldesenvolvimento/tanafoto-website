$(function() {
	$('form').on('keyup', 'input[name="telefone"], input[name="celular"]', function() {
		var phone, element;
		element = $(this);
		element.unmask();
		phone = element.val().replace(/\D/g, '');

		if (phone.length > 10)
			element.mask("(99) 99999-9999");
		else
			element.mask('(99) 9999-9999Z', {translation: {'Z': {pattern: /[0-9]/, optional: true}}});
	});
});
