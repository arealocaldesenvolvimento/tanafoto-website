=== AdRotate ===
Contributors: adegans
Donate link: https://www.arnan.me/donate.html?pk_campaign=adrotatefree&pk_keyword=readme
Tags: adverts, ads, banners, advert manager, ad manager, banner manager, monetise, revenue, place banners, adsense, dfp, doubleclick, amazon, affiliate, referral
Requires at least: 4.6
Requires PHP: 5.6
Tested up to: 5.4
Stable tag: 5.7
License: GPLv3

Manage your advertisements the easy way! Monetise your website with AdRotate. AdRotate has everything you need and keeps management simple!

== Description ==

With AdRotate you can easily place advertising banners pretty much anywhere on your website while managing everything from the easy to use dashboard. No fussing with your themes code or complex functions if you don't want to.

Easily create your own adverts with basic HTML and/or Javascript code or use adverts from your favorite Ad Server such as Media.net, Blind Ferret, Yahoo! Adverts, DFP, Google AdSense, Google Ad Manager, Bing Ads, Amazon Affiliates, Chitika, Doubleclick, JuiceAds and many more similar ad servers and affiliate programs.

> AdRotate is compatible with every advert you have, all managed from your dashboard. 

Including Google's new Auto-Ads, Javascript adverts, HTML5, VAST and even old-school Flash adverts.

AdRotate looks and feels similar to the WordPress dashboard you already know, this means you're already familiar with AdRotate before you even start using it. Familiarize yourself with the many useful features and you'll be up and running in minutes.

<strong>Getting started</strong>
Get your ad campaigns online in minutes. Simply install the plugin, explore the dashboard pages and new options and start creating ads.
If you need a hand installing AdRotate or you just want someone to handle the initial setup. Take a look at these [services](https://ajdg.solutions/services/?pk_campaign=adrotatefree&pk_keyword=readme).

Getting started with AdRotate is not complex, but a little help or advise is never bad.

**Some of the AdRotate Features**

* Works with ad servers such as; Media.net, Blind Ferret, Google, Bing, Amazon, DFP and most other referrer/ad servers
* Put random, or selected, banners in pages or posts
* Easy management of Adverts, Groups and Schedules
* Track how many times a banner is clicked and show it's Cick-Through-Ratio (CTR)
* Automagically disable ads after they expire
* Use shortcodes, widgets or PHP to put ads on your site
* Automatic rotation of ads with Dynamic Groups
* Dashboard notifications when ads are about to expire or need attention
* Show multiple adverts at once in a grid, column or row
* Any size advertisement, including 125x125, 468x60, 729x90, 160x600 and every common format you can imagine
* Easy to read stats so you can follow how each advert is performing
* Automatically delete short running adverts and stats after they expire
* Compatible with responsive adverts
* Have your advertisers add/edit/manage their own adverts
* Geo Targeting for adverts in every country
* Mobile adverts (differentiate tablets from phones or desktops)
* Disguise adverts from ad blockers so they're less likely to be blocked
* Get email notifications when your adverts need you
* Couple adverts to users for personalized stats
* Preview banners when creating or editing them
* Advanced time schedules and restrictions you control
* AdRotate Geo, AJdG Solutions' exclusive Geo Targeting service
* Export statistics to CSV

AdRotate and AdRotate Professional share many features. But some features are available in AdRotate Professional only. Learn more about [AdRotate Professional](https://ajdg.solutions/product-category/adrotate-pro/?pk_campaign=adrotatefree&pk_keyword=readme) on my website.

== Installation ==

Installing the plugin is as easy as searching for "Arnan AdRotate" or simply "AdRotate" in your plugin dashboard and clicking "Install Now" from your dashboards plugin page. Just like every other plugin.
Once activated, a new menu appears called "AdRotate" in the dashboard menu from which you manage everything in AdRotate.

For more detailed instructions check out the [installation steps](https://ajdg.solutions/support/adrotate-manuals/installing-adrotate-on-your-website/?pk_campaign=adrotatefree&pk_keyword=readme) on the AdRotate website.

== Changelog ==

Be a Pro and get [AdRotate Professional](https://ajdg.solutions/product-category/adrotate-pro/?pk_campaign=adrotatefree&pk_keyword=readme)!

= AdRotate 5.7 =
* [fix] Vulnerability related to groups
* [fix] Asset sometimes gets deselected when editing adverts
* [new] Tooltips for useful tags when editing adverts
* [change] Dashboard tweaks

= AdRotate Professional 5.7.1 =
* [fix] Better organised $_SESSION data for duplicate adverts
* [fix] Date selection for exports not working for some users
* [fix] Date selection for group stats not working for some users
* [fix] Date selection for advert stats not working for some users
* [fix] Better error handling for Geo Targeting
* [fix] Vulnerability related to groups
* [fix] Better error handling when checking for updates
* [change] Dashboard tweaks

All recent changes are available on the [AdRotate Changelog](https://ajdg.solutions/support/adrotate-development/?pk_campaign=adrotatefree&pk_keyword=readme).

== Upgrade Notice ==

Enjoy this latest update with the latest tweaks and fixes to further improve AdRotate for WordPress!

== Frequently Asked Questions ==

= Can I migrate my data from another plugin to AdRotate? =
Maybe. Take a look at [AdRotate Switch](https://ajdg.solutions/product/adrotate-switch/?pk_campaign=adrotatefree&pk_keyword=readme) and see if your current advertising plugin is compatible for migrating your data!

= How do I use AdRotate =
There are [user guides](https://ajdg.solutions/support/adrotate-manuals/?pk_campaign=adrotatefree&pk_keyword=readme) with every popular feature explained.
You can also post your questions on the [forum](https://ajdg.solutions/forums/?pk_campaign=adrotatefree&pk_keyword=readme).

= Why do some dashboard notifications look so ugly =
If a dashboard notification misses it's layout or looks out of shape try clearing your browser cache.
Some adblockers block parts of the AdRotate dashboard, check out this page to make an exception for your website in adblockers - [Whitelist your site](https://ajdg.solutions/support/adrotate-manuals/configure-adblockers-for-your-own-website/?pk_campaign=adrotatefree&pk_keyword=readme).

= Is AdRotate compatible with Yoast SEO or other SEO plugins? =
Yes, Yoast SEO, All in One SEO pack and all other SEO plugins work fine with AdRotate. 

= Is Jetpack compatible with AdRotate? =
Yes. 

= Does AdRotate work alongside caching plugins? =
AdRotate works best with Borlabs Cache and W3 Total Cache.
Other plugins such as WP Super Cache or WP Fastest Cache may work, but are untested.

= Can I use my adverts from Google AdSense? =
Yes, usually you can use their code as-is.
Even Googles new Auto-Ads are supported.

= Does AdRotate support HTML5 adverts? =
Yes!

= Does AdRotate work with WooCommerce? =
Yes!

= Can I use bbPress alongside AdRotate? =
Yes!

= Does AdRotate offer click tracking? =
Yes, and it counts impressions and CTR as well.

= Can I place adverts on forms from Contact Form 7? =
All contact form plugins should work fine.

= Are emails sent by AdRotate sent through my installed SMTP plugin? =
Yes! SMTP Mailer and similar plugins take over the wp_mail function which is what AdRotate uses.

== Screenshots ==

1. Creating / Editing adverts
2. Managing adverts
3. Scheduling adverts
4. Managing groups of adverts