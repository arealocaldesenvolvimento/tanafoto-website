<?php
/* Carrega o wordpress */
require_once '../../../wp-load.php';
require_once '../../../wp-admin/includes/image.php';

global $wpdb;

ini_set('max_execution_time', 300);

$sql = 'SELECT * FROM old_gallery ORDER BY data DESC LIMIT 7';

$results = $wpdb->get_results($sql);

// die;
foreach($results as $old_post) { 

    $data = new DateTime($old_post->data);
    $post_arr = array(
        'post_author' => 1,
        'post_date' => $data->format('Y-m-d H:i:s'),
        // 'post_date_gmt' => '',
        'post_title' => $old_post->post_title,
        'post_status' => 'publish',
        'post_type' => 'coberturas',
    );

    $imagens = unserialize($old_post->galeria);

    // echo '<pre>';
    // print_r($imagens);
    // print_r(wp_basename($imagens[0]));
    // echo '</pre>';

    $post_id = wp_insert_post($post_arr, true);

    $attach_ids = insertPostImages($post_id, $imagens, $data->format('Y'), $data->format('m'));
    
    set_post_thumbnail($post_id, $attach_ids[0]);
    add_post_meta($post_id, 'area_destaque', 0, true);
    add_post_meta($post_id, 'galeria', $attach_ids, true);
    add_post_meta($post_id, 'data', $data->format('Y-m-d H:i:s'), true);

    echo '<pre>';
    print_r($attach_ids);
    echo '</pre>';
    
}

/* ====== FUNÇÕES ====== */

function insertPostImages($post_id, $images, $ano, $mes){
    $attach_ids = array();
    // echo '<pre>';
    // print_r($images);
    // echo '</pre>';

    if(is_array($images)){

        foreach ($images as $key => $image) {
            $filename = 'old_images/'.$ano.'/'.$mes.'/'.wp_basename($image);
            echo $filename. '<br>';
            $filetype = wp_check_filetype(basename($filename), null);
            // $filetype = wp_check_filetype(basename($image), null);

            $wp_upload_dir = wp_upload_dir();
            $attachment = array(
                'guid'           => $wp_upload_dir['basedir'].'/'.$filename,
                // 'guid'           => $image,
                'post_mime_type' => $filetype['type'],
                'post_title'     => preg_replace('/\.[^.]+$/', '', basename($filename)),
                // 'post_title'     => preg_replace('/\.[^.]+$/', '', basename($image)),
                'post_content'   => '',
                'post_status'    => 'inherit'
            );
    
            $attach_id = wp_insert_attachment($attachment, $filename, $post_id);
            // $attach_id = wp_insert_attachment($attachment, $image, $post_id);
            $attach_data = wp_generate_attachment_metadata($attach_id, $attachment["guid"]);    
            wp_update_attachment_metadata($attach_id, $attach_data);

            $attach_ids[] = $attach_id;
        }
    }

    return $attach_ids;
}

?>