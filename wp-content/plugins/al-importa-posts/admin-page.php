<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Language" content="pt-br">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Exemplo</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.min.js"></script>
    <script src="http://paramquery.com/Content/js/pqgrid.min.js"></script>
    <script src="https://rawgit.com/emn178/js-md5/master/build/md5.min.js"></script>

    <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>

    <link href="http://paramquery.com/Content/css/pqgrid.min.css" type="text/css" rel="stylesheet">
    <link href="http://paramquery.com/Content/css/themes/Office/pqgrid.css" type="text/css" rel="stylesheet">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.31.0/sweetalert2.all.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.31.0/sweetalert2.min.css" />

    <script>

        function enviaSolicitacao() {
            
        };

    </script>
    <style>
        body{
            background: #e4e4e4;
        }
        a#enviaSolicitacao{
            background: #fcae46;
            color: #ececec;
            border: none;
            border-radius: 8px;
            font-weight: 700;
            font-size: 18px;
            transition: all 0.3s;
        }
        a#enviaSolicitacao:hover{
            color: #003f5a;
            background: #ececec;
        }
        #wpcontent{
            padding-left: 0 !important;
        }
        div#capa{
            color: #fff;
            text-align: center;
            font-family: monospace;
            text-shadow: 1px 1px 20px #000;
            background: url("<?= get_image_url('capa.png'); ?>") no-repeat center fixed;
            background-size: cover;
        }
        div#importador-content{
            padding: 25px 25px 25px 25px;
            text-align: center;
        }
        div#importador-content pre{
            text-align: left;
        }
        div.yoast-alert{
            display: none;
        }
    </style>
</head>
<body>
    <div id="capa" style="width: 100%; padding: 75px 30px;" >
        <h1>IMPORTADOR DE POSTS</h1>
        <p>Esta ferramenta importa a lista atualizada de posts, ou coberturas.<br>
        <strong>AVISO!</strong>: Depois de iniciado o procedimento de importação não pode ser parado, é irreversível e pode demorar alguns minutos para ser concluído.</p>
    </div>    
    <div id="importador-content">
        <a href="<?= get_home_url() ?>/wp-content/plugins/al-importa-posts/importador.php" id="enviaSolicitacao" name="enviaSolicitacao" class="btn btn-success btn-lg" aria-label="Salvar" style="width: 200px">Importar Coberturas</a>
    </div>
    <form id='produtos' method="post"><input type="hidden" name="produtos"></form>
</body>
</html>
