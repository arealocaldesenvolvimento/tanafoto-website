<?php
/*
Plugin Name: Al-Importador de Posts
Description: Importa os posts do site antigo Tanafoto.
Author: Área Local
*/

add_action( 'admin_menu', 'menuImportacaoPosts' );

function menuImportacaoPosts()
{
      add_menu_page(
        'Importação de Posts',
        'Importar Posts',
        'manage_options',
        'importador.php',
        'importador_admin_page',
        'dashicons-tickets'
    );
}




function importador_admin_page(){
    require_once plugin_dir_path(__FILE__).'admin-page.php';
}