<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Language" content="pt-br">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Exemplo</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.min.js"></script>

    <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.31.0/sweetalert2.all.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.31.0/sweetalert2.min.css" />

    <!-- Datatbles -->
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css" />
    <script src="//cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>

    <style>
        #table-promocoes_wrapper{
            margin-top: 50px;
        }
    </style>

    <script>

        function listaParticipantes() {
            $.ajax({
                method: "POST",
                url: "<?= plugins_url('lista-promocao.php',plugin_dir_path(__FILE__).'lista-promocao.php') ?>"
            }).done(function (data) {
                var dados = JSON.parse(data);
                var html = '';
                $.each(dados, function(index, value){
                    html += '<tr>';
                    html +=     '<td>'+value.post_title+'</td>';
                    html +=     '<td>'+value.nome+'</td>';
                    html +=     '<td>'+value.email+'</td>';
                    html += '</tr>';
                });

                $('#body-table-promocoes').append(html);
                $('#table-promocoes').DataTable();

            }).fail(function (data) {
                console.log(data);
                Swal({
                  type: 'error',
                  title: 'Erro',
                  text: 'Nao foi possível carregar os dados das promoções'
                });
            });
        };

        $(document).ready(function(){
            listaParticipantes();
        });
    </script>
</head>
<body>    
    <table id="table-promocoes" class="display" style="width:100%; margin-top: 50px;">
        <thead>
            <tr>
                <th>Promoção</th>
                <th>Nome</th>
                <th>Email</th>
            </tr>
        </thead>
        <tbody id="body-table-promocoes">

        </tbody>
        <tfoot>
            <tr>
                <th>Promoção</th>
                <th>Nome</th>
                <th>Email</th>
            </tr>
        </tfoot>
    </table>
</body>
</html>
