<?php
require('../../../wp-load.php');
global $wpdb;

$sql = "
    SELECT {$wpdb->prefix}promocao.*, {$wpdb->prefix}posts.post_title
    FROM {$wpdb->prefix}promocao
    JOIN {$wpdb->prefix}posts on({$wpdb->prefix}posts.ID = {$wpdb->prefix}promocao.post_id)
    ORDER BY {$wpdb->prefix}posts.post_date DESC
    ";
$result = $wpdb->get_results( $sql );

echo json_encode($result);

