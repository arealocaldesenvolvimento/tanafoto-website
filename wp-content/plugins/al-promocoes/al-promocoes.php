<?php
/*
Plugin Name: Al-Promoções
Description: Mostra os inscritos nas promoções.
Author: Área Local
*/

add_action( 'admin_menu', 'menuPromocoes' );

function menuPromocoes()
{
      add_menu_page(
        'Promoções',
        'Ver Promoções',
        'manage_options',
        'lista-promocao.php',
        'promocoes_admin_page',
        'dashicons-tickets'
    );
}

function promocoes_admin_page(){
    require_once plugin_dir_path(__FILE__).'admin-page.php';
}