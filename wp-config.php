<?php
/**
 * As configurações básicas do WordPress
 *
 * O script de criação wp-config.php usa esse arquivo durante a instalação.
 * Você não precisa usar o site, você pode copiar este arquivo
 * para "wp-config.php" e preencher os valores.
 *
 * Este arquivo contém as seguintes configurações:
 *
 * * Configurações do MySQL
 * * Chaves secretas
 * * Prefixo do banco de dados
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/pt-br:Editando_wp-config.php
 *
 * @package WordPress
 */

// ** Configurações do MySQL - Você pode pegar estas informações com o serviço de hospedagem ** //
/** O nome do banco de dados do WordPress */
define('DB_NAME', 'tanafoto_db');

/** Usuário do banco de dados MySQL */
define('DB_USER', 'root');

/** Senha do banco de dados MySQL */
define('DB_PASSWORD', 'root');

/** Nome do host do MySQL */
define('DB_HOST', '192.168.0.39');

/** Charset do banco de dados a ser usado na criação das tabelas. */
define('DB_CHARSET', 'utf8');

/** O tipo de Collate do banco de dados. Não altere isso se tiver dúvidas. */
define('DB_COLLATE', '');

/**#@+
 * Chaves únicas de autenticação e salts.
 *
 * Altere cada chave para um frase única!
 * Você pode gerá-las
 * usando o {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org
 * secret-key service}
 * Você pode alterá-las a qualquer momento para invalidar quaisquer
 * cookies existentes. Isto irá forçar todos os
 * usuários a fazerem login novamente.
 *
 * @since 2.6.0
 */
 define('AUTH_KEY',         '?8f?G$X1bz:_<ym-ln$Vb_pOD/546r;d>WuR%a/K@+|*@1BhbH%n-^4O)eh!w5NB');
 define('SECURE_AUTH_KEY',  'oq>-&>XibFf4jFP}zsL`aC3EJ2@qM=geS({pMF;d-%q]+;ZDc|+|{p2-Q5YiBYXJ');
 define('LOGGED_IN_KEY',    'k^RO1YUe:/5%+9K!Q&OgSu=>~D|*[A~[}MqQ#B9~00GUL!<i=^`(4^U,3G`D@25;');
 define('NONCE_KEY',        'Gt_p-NJ*0O5/z8&Kg=h.CYo<TW^:zzE!-i09|$jG,6%}E4[wXrTkb{-e[o@Q/%nO');
 define('AUTH_SALT',        'O`!A5$%n0l|aJ_rJg5|_o:4!>gJ+*:u|PBTrAw HxPM|D7e90:PTp,Q)H)q%7xK=');
 define('SECURE_AUTH_SALT', 'IYwhN|9z$l|ynNj&SGHs%6QZgk-{x-Q~^5Yrzlnxxxqp1~E`EWQ-L06E_P_Vj$_*');
 define('LOGGED_IN_SALT',   's|2$HJ?fvOpSgvd%_-{FITn{XB<+V-N5glUDCB30U?w$EwA <jaw?5PD!T:&/ti<');
 define('NONCE_SALT',       'v[!2yegq|lV 9+]j&}vc:-p(YS>>j%X:S6Syn%|C ][J..G%Hus2DvP)]eFxXZ4u');

/**#@-*/

/**
 * Prefixo da tabela do banco de dados do WordPress.
 *
 * Você pode ter várias instalações em um único banco de dados se você der
 * um prefixo único para cada um. Somente números, letras e sublinhados!
 */
$table_prefix  = 'al_';

/**
 * Para desenvolvedores: Modo de debug do WordPress.
 *
 * Altere isto para true para ativar a exibição de avisos
 * durante o desenvolvimento. É altamente recomendável que os
 * desenvolvedores de plugins e temas usem o WP_DEBUG
 * em seus ambientes de desenvolvimento.
 *
 * Para informações sobre outras constantes que podem ser utilizadas
 * para depuração, visite o Codex.
 *
 * @link https://codex.wordpress.org/pt-br:Depura%C3%A7%C3%A3o_no_WordPress
 */
define('WP_DEBUG', false);
define('FS_METHOD','direct');

/* Isto é tudo, pode parar de editar! :) */

/** Caminho absoluto para o diretório WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Configura as variáveis e arquivos do WordPress. */
require_once(ABSPATH . 'wp-settings.php');
